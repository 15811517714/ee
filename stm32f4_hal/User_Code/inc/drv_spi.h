#ifndef __DRV_SPI_H
#define __DRV_SPI_H

#include "stm32f4xx.h"

#define USE_SPI1
//#define USE_SPI2
//#define USE_SPI3

#ifdef  USE_SPI1
extern  SPI_HandleTypeDef  hspi1;
#endif

#ifdef  USE_SPI2
extern  SPI_HandleTypeDef  hspi2;
#endif

#ifdef  USE_SPI3
extern  SPI_HandleTypeDef  hspi3;
#endif

void SPI_Init(void);
void SPI_SetSpeed(SPI_HandleTypeDef *hspix, unsigned char SPI_BaudRatePrescaler);
unsigned char SPI_RW_Byte(SPI_HandleTypeDef *hspix, unsigned char byte);

#endif
