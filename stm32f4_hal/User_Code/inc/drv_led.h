#ifndef __DRV_LED_H
#define __DRV_LED_H

#include "stm32f4xx.h"

#define  LED_GPIO  GPIOC
#define  LED_PIN   GPIO_PIN_13

void LED_Init(void);

#endif
