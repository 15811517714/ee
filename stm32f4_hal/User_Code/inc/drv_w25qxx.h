#ifndef __DRV_W25QXX_H
#define __DRV_W25QXX_H

#include "stm32f4xx.h"

#define  CMD_Write_EN          0x06
#define  CMD_Write_DIS         0x04
#define  CMD_Read_SR1          0x05
#define  CMD_Read_SR2          0x35
#define  CMD_Write_SR          0x01
#define  CMD_Page_Program      0x02
#define  CMD_QuadPageProgram   0x32
#define  CMD_Block_Erase64     0xD8
#define  CMD_Block_Erase32     0x52
#define  CMD_Sector_Erase      0x20
#define  CMD_Chip_Erase        0xC7
#define  CMD_Erase_Suspend     0x75
#define  CMD_Erase_Resume      0x7A
#define  CMD_Power_Down        0xB9
#define  CMD_High_Performace   0xA3
#define  CMD_Continue_Read     0xFF
#define  CMD_Release_HPM       0xAB
#define  CMD_Device_ID         0x90
#define  CMD_Unique_ID         0x4B
#define  CMD_JEDEC_ID          0x9F
#define  CMD_Read_Data         0x03
#define  CMD_Fast_Read         0x0B
#define  CMD_Read_DualOut      0x3B
#define  CMD_Read_DualIO       0xBB
#define  CMD_Read_DuadOut      0x6B
#define  CMD_Read_DuadIO       0xEB
#define  CMD_Octal_Word_Read   0xE3

#define  Dummy_Byte            0xFF
#define  Page_Size             0x100
#define  Sector_Size           0x1000
#define  Block_Size            0x10000

void W25Qx_Select(void);
void W25Qx_Deselect(void);
void W25Qx_WaitFree(void);
void W25Qx_PowerDown(void);
uint8_t W25Qx_ReadSR1(void);
uint8_t W25Qx_ReadSR2(void);
uint16_t W25Qx_ReadSR(void);
void W25Qx_ChipErase(void);
void W25Qx_WriteEnable(void);
void W25Qx_WriteDisable(void);
void W25Qx_ReleasePowerDown(void);
uint16_t W25Qx_ReadDevice_ID(void);
uint64_t W25Qx_ReadUnique_ID(void);
uint32_t W25Qx_ReadJEDEC_ID(void);
void W25Qx_WriteSR(uint16_t SR_value);
void W25Qx_SetSpeed(uint8_t SpeedSet);
uint8_t W25Qx_WriteByte(uint8_t byte);
void W25Qx_SectorErase(uint32_t SectorAddr);
void W25Qx_BlockErase32(uint32_t BlockAddr);
void W25Qx_BlockErase64(uint32_t BlockAddr);
void W25Qx_PageProgram(uint8_t *pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite);
void W25Qx_WriteBuffer(uint8_t *pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite);
void W25Qx_ReadBuffer(uint8_t *pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead);

#endif
