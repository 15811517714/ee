#ifndef __DRV_MX25XX_H
#define __DRV_MX25XX_H

#include "stm32f4xx.h"

#define  CMD_Write_EN          0x06
#define  CMD_Write_DIS         0x04
#define  CMD_Read_SR1          0x05
#define  CMD_Read_SR2          0x35
#define  CMD_Read_CR           0x15
#define  CMD_Write_SR          0x01
#define  CMD_Page_Program      0x02
#define  CMD_QuadPageProgram   0x32
#define  CMD_Block_Erase64     0xD8
#define  CMD_Block_Erase32     0x52
#define  CMD_Sector_Erase      0x20
#define  CMD_Chip_Erase        0xC7
#define  CMD_Erase_Suspend     0x75
#define  CMD_Erase_Resume      0x7A
#define  CMD_Power_Down        0xB9
#define  CMD_High_Performace   0xA3
#define  CMD_Continue_Read     0xFF
#define  CMD_Release_HPM       0xAB
#define  CMD_Device_ID         0x90
#define  CMD_Unique_ID         0x4B
#define  CMD_JEDEC_ID          0x9F
#define  CMD_Read_Data         0x03
#define  CMD_Fast_Read         0x0B
#define  CMD_Read_DualOut      0x3B
#define  CMD_Read_DualIO       0xBB
#define  CMD_Read_DuadOut      0x6B
#define  CMD_Read_DuadIO       0xEB
#define  CMD_Octal_Word_Read   0xE3

#define  Dummy_Byte            0xFF
#define  Page_Size             0x100
#define  Sector_Size           0x1000
#define  Block_Size            0x10000

void MX25xx_Select(void);
void MX25xx_Deselect(void);
void MX25xx_WaitFree(void);
void MX25xx_PowerDown(void);
uint8_t MX25xx_ReadSR1(void);
uint8_t MX25xx_ReadSR2(void);
uint16_t MX25xx_ReadSR(void);
uint8_t MX25xx_ReadCR(void);
void MX25xx_ChipErase(void);
void MX25xx_WriteEnable(void);
void MX25xx_WriteDisable(void);
void MX25xx_ReleasePowerDown(void);
uint16_t MX25xx_ReadDevice_ID(void);
uint64_t MX25xx_ReadUnique_ID(void);
uint32_t MX25xx_ReadJEDEC_ID(void);
void MX25xx_WriteSR(uint16_t SR_value);
void MX25xx_SetSpeed(uint8_t SpeedSet);
uint8_t MX25xx_WriteByte(uint8_t byte);
void MX25xx_SectorErase(uint32_t SectorAddr);
void MX25xx_BlockErase32(uint32_t BlockAddr);
void MX25xx_BlockErase64(uint32_t BlockAddr);
void MX25xx_PageProgram(uint8_t *pBuffer, uint32_t WriteAddr, uint32_t NumByteToWrite);
void MX25xx_WriteBuffer(uint8_t *pBuffer, uint32_t WriteAddr, uint32_t NumByteToWrite);
void MX25xx_ReadBuffer(uint8_t *pBuffer, uint32_t ReadAddr, uint32_t NumByteToRead);
uint8_t MX25xx_ReadBufferWithCheck(uint8_t *pBuffer, uint32_t ReadAddr, uint32_t NumByteToRead);
uint8_t MX25xx_WriteBufferWithCheck(uint8_t *pBuffer, uint32_t WriteAddr, uint32_t NumByteToWrite, uint8_t crc8);

#endif
