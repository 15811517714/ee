/*<FH+>************************************************************************/
/*                                                                            */
/* 版权所有: Copyright (C) 烽鸟出行. 2019. All rights reserved.               */
/*                                                                            */
/* 文件名称: drv_w25qxx.c                                                     */
/* 内容摘要: W25Qxx芯片驱动                                                   */
/* 其它说明:                                                                  */
/* 当前版本: V0.1                                                             */
/* 作    者: Leonard                                                          */
/* 完成日期: 2019-10-01                                                       */
/* 修改记录:                                                                  */
/*                                                                            */
/*<FH->************************************************************************/
#include <stdio.h>
#include "drv_spi.h"
#include "drv_w25qxx.h"

#define  W25Qxx_CE  GPIO_PIN_4
#define  W25Qxx_GP  GPIOA

/* W25Qxx on the SPI1 */
SPI_HandleTypeDef  *SPI_w25qxx = &hspi1;

void W25Qx_Select(void)
{
    W25Qxx_GP -> BSRR = W25Qxx_CE << 16;
    __nop();__nop();__nop();__nop();__nop();
}

void W25Qx_Deselect(void)
{
    W25Qxx_GP -> BSRR = W25Qxx_CE;
    __nop();__nop();__nop();__nop();__nop();
}

void W25Qx_WriteEnable(void)
{
    W25Qx_Select();
    SPI_RW_Byte(SPI_w25qxx, CMD_Write_EN);
    W25Qx_Deselect();
}

void W25Qx_WriteDisable(void)   
{  
    W25Qx_Select();
    SPI_RW_Byte(SPI_w25qxx, CMD_Write_DIS);
    W25Qx_Deselect();  	      
} 

uint8_t W25Qx_ReadSR1(void)
{
    uint8_t SR_value;
    W25Qx_Select();
    SPI_RW_Byte(SPI_w25qxx, CMD_Read_SR1);
    SR_value = SPI_RW_Byte(SPI_w25qxx, Dummy_Byte);
    W25Qx_Deselect();
    return SR_value;
}

uint8_t W25Qx_ReadSR2(void)
{
    uint8_t SR_value;
    W25Qx_Select();
    SPI_RW_Byte(SPI_w25qxx, CMD_Read_SR2);
    SR_value = SPI_RW_Byte(SPI_w25qxx, Dummy_Byte);
    W25Qx_Deselect();
    return SR_value;
}
 
uint16_t W25Qx_ReadSR(void)
{
    return (((uint16_t)W25Qx_ReadSR1() << 8) | W25Qx_ReadSR2());
}

void W25Qx_WaitFree(void)
{
    W25Qx_Select();
    SPI_RW_Byte(SPI_w25qxx, CMD_Read_SR1);
    while (SPI_RW_Byte(SPI_w25qxx, Dummy_Byte) & 0x01){}
    W25Qx_Deselect();     
}

void W25Qx_WriteSR(uint16_t SR_value)   
{  
    W25Qx_WriteEnable();
    W25Qx_Select();                 
    SPI_RW_Byte(SPI_w25qxx, CMD_Write_SR);
    SPI_RW_Byte(SPI_w25qxx, (uint8_t)(SR_value & 0x00FF));  
    SPI_RW_Byte(SPI_w25qxx, (uint8_t)(SR_value >> 8)); 
    W25Qx_Deselect(); 
    W25Qx_WaitFree();
} 

void W25Qx_ReadBuffer(uint8_t *pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead)
{
    W25Qx_Select();   
    SPI_RW_Byte(SPI_w25qxx, CMD_Read_Data);
    //SPI_RW_Byte(SPI_w25qxx, CMD_Fast_Read);  // Fast Read 0Bh
    SPI_RW_Byte(SPI_w25qxx, (ReadAddr & 0xFF0000) >> 16);
    SPI_RW_Byte(SPI_w25qxx, (ReadAddr & 0xFF00) >> 8);
    SPI_RW_Byte(SPI_w25qxx,  ReadAddr & 0xFF);
    //SPI_RW_Byte(SPI_w25qxx, Dummy_Byte);  // Fast Read 0Bh
    while( NumByteToRead-- ) 
    {
        *pBuffer = SPI_RW_Byte(SPI_w25qxx, Dummy_Byte);
         pBuffer++;
    }
    W25Qx_Deselect();     
}

void W25Qx_PageProgram(uint8_t *pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
    W25Qx_WriteEnable();
    W25Qx_Select();
    SPI_RW_Byte(SPI_w25qxx, CMD_Page_Program);
    SPI_RW_Byte(SPI_w25qxx, (WriteAddr & 0xFF0000) >> 16);
    SPI_RW_Byte(SPI_w25qxx, (WriteAddr & 0xFF00) >> 8);
    SPI_RW_Byte(SPI_w25qxx,  WriteAddr & 0xFF);

    if(NumByteToWrite > Page_Size)
    {
        NumByteToWrite = Page_Size;
    }

    while( NumByteToWrite-- )
    {
        SPI_RW_Byte(SPI_w25qxx, *pBuffer);
        pBuffer++;
    }

    W25Qx_Deselect();     
    W25Qx_WaitFree();
}

void W25Qx_WriteBuffer(uint8_t *pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
    uint8_t NumOfPage = 0, NumOfSingle = 0, Addr = 0, count = 0, temp = 0;

    Addr = WriteAddr % Page_Size;
    count = Page_Size - Addr;
    NumOfPage =  NumByteToWrite / Page_Size;
    NumOfSingle = NumByteToWrite % Page_Size;

    if (Addr == 0) // WriteAddr is aligned 
    {
        if (NumOfPage == 0)  // NumByteToWrite < Page_Size
        {
            W25Qx_PageProgram(pBuffer, WriteAddr, NumByteToWrite);
        }
        else  // NumByteToWrite >= Page_Size
        {
            while (NumOfPage--)
            {
                W25Qx_PageProgram(pBuffer, WriteAddr, Page_Size);
                WriteAddr += Page_Size;
                pBuffer += Page_Size;
            }

            W25Qx_PageProgram(pBuffer, WriteAddr, NumOfSingle);
        }
    }
    else // WriteAddr is not aligned 
    {
        if (NumOfPage == 0) // NumByteToWrite < Page_Size
        {
            if (NumOfSingle > count) // (NumByteToWrite + WriteAddr) > Page_Size
            {
                temp = NumOfSingle - count;

                W25Qx_PageProgram(pBuffer, WriteAddr, count);
                WriteAddr +=  count;
                pBuffer += count;

                W25Qx_PageProgram(pBuffer, WriteAddr, temp);
            }
            else
            {
                W25Qx_PageProgram(pBuffer, WriteAddr, NumByteToWrite);
            }
        }
        else // NumByteToWrite >= Page_Size
        {
            NumByteToWrite -= count;
            NumOfPage =  NumByteToWrite / Page_Size;
            NumOfSingle = NumByteToWrite % Page_Size;

            W25Qx_PageProgram(pBuffer, WriteAddr, count);
            WriteAddr +=  count;
            pBuffer += count;

            while (NumOfPage--)
            {
                W25Qx_PageProgram(pBuffer, WriteAddr, Page_Size);
                WriteAddr +=  Page_Size;
                pBuffer += Page_Size;
            }

            if (NumOfSingle != 0)
            {
                W25Qx_PageProgram(pBuffer, WriteAddr, NumOfSingle);
            }
        }
    }
}

void W25Qx_SectorErase(uint32_t SectorAddr) // 4kB
{
    W25Qx_WriteEnable();
    W25Qx_Select();   
    SPI_RW_Byte(SPI_w25qxx, CMD_Sector_Erase);
    SPI_RW_Byte(SPI_w25qxx, (SectorAddr & 0xFF0000) >> 16);
    SPI_RW_Byte(SPI_w25qxx, (SectorAddr & 0xFF00) >> 8);
    SPI_RW_Byte(SPI_w25qxx,  SectorAddr & 0xFF);
    W25Qx_Deselect(); 
    W25Qx_WaitFree();
}

void W25Qx_BlockErase32(uint32_t BlockAddr) 
{
    W25Qx_WriteEnable();
    W25Qx_Select();   
    SPI_RW_Byte(SPI_w25qxx, CMD_Block_Erase32);
    SPI_RW_Byte(SPI_w25qxx, (BlockAddr & 0xFF0000) >> 16);
    SPI_RW_Byte(SPI_w25qxx, (BlockAddr & 0xFF00) >> 8);
    SPI_RW_Byte(SPI_w25qxx,  BlockAddr & 0xFF);
    W25Qx_Deselect(); 
    W25Qx_WaitFree();
}

void W25Qx_BlockErase64(uint32_t BlockAddr) 
{
    W25Qx_WriteEnable();
    W25Qx_Select();   
    SPI_RW_Byte(SPI_w25qxx, CMD_Block_Erase64);
    SPI_RW_Byte(SPI_w25qxx, (BlockAddr & 0xFF0000) >> 16);
    SPI_RW_Byte(SPI_w25qxx, (BlockAddr & 0xFF00) >> 8);
    SPI_RW_Byte(SPI_w25qxx,  BlockAddr & 0xFF);
    W25Qx_Deselect(); 
    W25Qx_WaitFree();
}

void W25Qx_ChipErase(void) 
{
    W25Qx_WriteEnable();
    W25Qx_Select();   
    SPI_RW_Byte(SPI_w25qxx, CMD_Chip_Erase);
    W25Qx_Deselect(); 
    W25Qx_WaitFree();
}

void W25Qx_PowerDown(void)  //lower power
{
    uint8_t delay = 0;
    W25Qx_Select();   
    SPI_RW_Byte(SPI_w25qxx, CMD_Power_Down);
    W25Qx_Deselect();   
    while(delay++ < 24);   //5us	
}

void W25Qx_ReleasePowerDown(void)
{
    uint8_t delay = 0;
    W25Qx_Select();   
    SPI_RW_Byte(SPI_w25qxx, CMD_Release_HPM);
    W25Qx_Deselect(); 
    while(delay++ < 24);   //5us	 
}

uint16_t W25Qx_ReadDevice_ID(void)
{
    uint16_t temp0 = 0, temp1 = 0;
    W25Qx_Select();   
    SPI_RW_Byte(SPI_w25qxx, CMD_Device_ID);
    SPI_RW_Byte(SPI_w25qxx, 0x00);
    SPI_RW_Byte(SPI_w25qxx, 0x00);
    SPI_RW_Byte(SPI_w25qxx, 0x00);
    temp0 = SPI_RW_Byte(SPI_w25qxx, Dummy_Byte);
    temp1 = SPI_RW_Byte(SPI_w25qxx, Dummy_Byte);
    W25Qx_Deselect();  
    printf("Manufacturer ID is %x!\nDevice ID is %x!\n", temp0, temp1);
    temp0 = (temp0 << 8) | temp1;
    return temp0;
}

uint64_t W25Qx_ReadUnique_ID(void)
{
    uint32_t ID64 = 0, i = 0;
    W25Qx_Select();
    SPI_RW_Byte(SPI_w25qxx, CMD_Unique_ID);
    while(i++ < 12)
    {
        if(i < 5)
        {
            SPI_RW_Byte(SPI_w25qxx, Dummy_Byte);
        }
        else
        {
            ID64 = (ID64 << 8) | SPI_RW_Byte(SPI_w25qxx, Dummy_Byte);
        }
    }
    W25Qx_Deselect();  
    printf("Unique ID is %x!\n", ID64);
    return ID64;
}

uint32_t W25Qx_ReadJEDEC_ID(void)
{
    uint32_t temp = 0, temp0 = 0, temp1 = 0, temp2 = 0;
    W25Qx_Select();
    SPI_RW_Byte(SPI_w25qxx, CMD_JEDEC_ID);
    temp0 = SPI_RW_Byte(SPI_w25qxx, Dummy_Byte);
    temp1 = SPI_RW_Byte(SPI_w25qxx, Dummy_Byte);
    temp2 = SPI_RW_Byte(SPI_w25qxx, Dummy_Byte);
    W25Qx_Deselect();  
    printf("Manufacturer ID is %x!\nMemory Type ID is %x!\nCapacity ID is %x!\n", temp0, temp1, temp2);
    temp = (temp0 << 16) | (temp1 << 8) | temp2;
    return temp;
}
