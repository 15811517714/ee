/*<FH+>************************************************************************/
/*                                                                            */
/* 版权所有: Copyright (C) 烽鸟出行. 2019. All rights reserved.               */
/*                                                                            */
/* 文件名称: drv_spi.c                                                        */
/* 内容摘要: SPI外设初始化                                                    */
/* 其它说明:                                                                  */
/* 当前版本: V0.1                                                             */
/* 作    者: Leonard                                                          */
/* 完成日期: 2019-10-01                                                       */
/* 修改记录:                                                                  */
/*                                                                            */
/*<FH->************************************************************************/
#include "drv_spi.h"

#ifdef  USE_SPI1
    SPI_HandleTypeDef  hspi1;
#endif

#ifdef  USE_SPI2
    SPI_HandleTypeDef  hspi2;
#endif

#ifdef  USE_SPI3
    SPI_HandleTypeDef  hspi3;
#endif

void SPI_Init(void)
{
#ifdef  USE_SPI1
    /* Enable clock of SPI1 */
    __HAL_RCC_SPI1_CLK_ENABLE();
    /* Init SPI1 */
    hspi1.Instance = SPI1;
    hspi1.Init.Mode = SPI_MODE_MASTER;
    hspi1.Init.Direction = SPI_DIRECTION_2LINES;
    hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
    hspi1.Init.CLKPolarity = SPI_POLARITY_HIGH;
    hspi1.Init.CLKPhase = SPI_PHASE_2EDGE; 
    hspi1.Init.NSS = SPI_NSS_SOFT;
    hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
    hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
    hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
    hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    hspi1.Init.CRCPolynomial = 7;
    HAL_SPI_Init(&hspi1);
    /* Enable SPI1 */
    __HAL_SPI_ENABLE(&hspi1); 
#endif
    
#ifdef  USE_SPI2
    /* Enable clock of SPI2 */
    __HAL_RCC_SPI2_CLK_ENABLE();
    /* Init SPI2 */
    hspi2.Instance = SPI2;
    hspi2.Init.Mode = SPI_MODE_MASTER;
    hspi2.Init.Direction = SPI_DIRECTION_2LINES;
    hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
    hspi2.Init.CLKPolarity = SPI_POLARITY_HIGH;
    hspi2.Init.CLKPhase = SPI_PHASE_2EDGE; 
    hspi2.Init.NSS = SPI_NSS_SOFT;
    hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
    hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
    hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
    hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    hspi2.Init.CRCPolynomial = 7;
    HAL_SPI_Init(&hspi2);
    /* Enable SPI2 */
    __HAL_SPI_ENABLE(&hspi2); 
#endif

#ifdef  USE_SPI3
    /* Enable clock of SPI3 */
    __HAL_RCC_SPI3_CLK_ENABLE();
    /* Init SPI3 */
    hspi3.Instance = SPI3;
    hspi3.Init.Mode = SPI_MODE_MASTER;
    hspi3.Init.Direction = SPI_DIRECTION_2LINES;
    hspi3.Init.DataSize = SPI_DATASIZE_8BIT;
    hspi3.Init.CLKPolarity = SPI_POLARITY_HIGH;
    hspi3.Init.CLKPhase = SPI_PHASE_2EDGE; 
    hspi3.Init.NSS = SPI_NSS_SOFT;
    hspi3.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
    hspi3.Init.FirstBit = SPI_FIRSTBIT_MSB;
    hspi3.Init.TIMode = SPI_TIMODE_DISABLE;
    hspi3.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    hspi3.Init.CRCPolynomial = 7;
    HAL_SPI_Init(&hspi3);
    /* Enable SPI3 */
    __HAL_SPI_ENABLE(&hspi3); 
#endif
}

void SPI_SetSpeed(SPI_HandleTypeDef *hspix, unsigned char SPI_BaudRatePrescaler)
{
    assert_param(IS_SPI_BAUDRATE_PRESCALER(SPI_BaudRatePrescaler));

    __HAL_SPI_DISABLE(hspix);
    hspix->Instance->CR1 &= 0xFFC7;
    hspix->Instance->CR1 |= SPI_BaudRatePrescaler;
    __HAL_SPI_ENABLE(hspix); 
}

//unsigned char SPI_RW_Byte(SPI_HandleTypeDef *hspix, unsigned char byte)
//{
//    while((hspix->Instance->SR & SPI_FLAG_TXE) == RESET);
//    hspix->Instance->DR = byte;

//    while((hspix->Instance->SR & SPI_FLAG_RXNE) == RESET);
//    return hspix->Instance->DR;
//}

unsigned char SPI_RW_Byte(SPI_HandleTypeDef *hspix, unsigned char byte)
{
    unsigned char Rxdata;
    HAL_SPI_TransmitReceive(hspix,&byte,&Rxdata,1, 1);       
 	return Rxdata;          		    //返回收到的数据		
}