#include "drv_led.h"

void LED_Init(void)
{
    GPIO_InitTypeDef  GPIO_Initure;
    
    __HAL_RCC_GPIOC_CLK_ENABLE();
	
    GPIO_Initure.Pin = LED_PIN;
    GPIO_Initure.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_Initure.Pull = GPIO_NOPULL;
    GPIO_Initure.Speed = GPIO_SPEED_HIGH;
    HAL_GPIO_Init(LED_GPIO, &GPIO_Initure);
}
