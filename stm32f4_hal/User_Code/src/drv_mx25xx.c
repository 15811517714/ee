/*<FH+>************************************************************************/
/*                                                                            */
/* 版权所有: Copyright (C) 烽鸟出行. 2019. All rights reserved.               */
/*                                                                            */
/* 文件名称: drv_mx25xx.c                                                     */
/* 内容摘要: MX25xx芯片驱动                                                   */
/* 其它说明:                                                                  */
/* 当前版本: V0.1                                                             */
/* 作    者: Leonard                                                          */
/* 完成日期: 2019-10-01                                                       */
/* 修改记录:                                                                  */
/*                                                                            */
/*<FH->************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "drv_spi.h"
#include "drv_mx25xx.h"

#define  MX25xx_CE  GPIO_PIN_4
#define  MX25xx_GP  GPIOA

/* MX25xx on the SPI2 */
SPI_HandleTypeDef  *SPI_mx25xx = &hspi1;

static const uint8_t crc8_table[256] =
{   0x00, 0x31, 0x62, 0x53, 0xC4, 0xF5, 0xA6, 0x97, 0xB9, 0x88, 
	0xDB, 0xEA, 0x7D, 0x4C, 0x1F, 0x2E, 0x43, 0x72, 0x21, 0x10, 
	0x87, 0xB6, 0xE5, 0xD4, 0xFA, 0xCB, 0x98, 0xA9, 0x3E, 0x0F, 
	0x5C, 0x6D, 0x86, 0xB7, 0xE4, 0xD5, 0x42, 0x73, 0x20, 0x11, 
	0x3F, 0x0E, 0x5D, 0x6C, 0xFB, 0xCA, 0x99, 0xA8, 0xC5, 0xF4, 
	0xA7, 0x96, 0x01, 0x30, 0x63, 0x52, 0x7C, 0x4D, 0x1E, 0x2F, 
	0xB8, 0x89, 0xDA, 0xEB, 0x3D, 0x0C, 0x5F, 0x6E, 0xF9, 0xC8, 
	0x9B, 0xAA, 0x84, 0xB5, 0xE6, 0xD7, 0x40, 0x71, 0x22, 0x13, 
	0x7E, 0x4F, 0x1C, 0x2D, 0xBA, 0x8B, 0xD8, 0xE9, 0xC7, 0xF6, 
    0xA5, 0x94, 0x03, 0x32, 0x61, 0x50, 0xBB, 0x8A, 0xD9, 0xE8, 
	0x7F, 0x4E, 0x1D, 0x2C, 0x02, 0x33, 0x60, 0x51, 0xC6, 0xF7, 
	0xA4, 0x95, 0xF8, 0xC9, 0x9A, 0xAB, 0x3C, 0x0D, 0x5E, 0x6F, 
	0x41, 0x70, 0x23, 0x12, 0x85, 0xB4, 0xE7, 0xD6, 0x7A, 0x4B, 
	0x18, 0x29, 0xBE, 0x8F, 0xDC, 0xED, 0xC3, 0xF2, 0xA1, 0x90, 
	0x07, 0x36, 0x65, 0x54, 0x39, 0x08, 0x5B, 0x6A, 0xFD, 0xCC, 
	0x9F, 0xAE, 0x80, 0xB1, 0xE2, 0xD3, 0x44, 0x75, 0x26, 0x17, 
	0xFC, 0xCD, 0x9E, 0xAF, 0x38, 0x09, 0x5A, 0x6B, 0x45, 0x74, 
	0x27, 0x16, 0x81, 0xB0, 0xE3, 0xD2, 0xBF, 0x8E, 0xDD, 0xEC, 
	0x7B, 0x4A, 0x19, 0x28, 0x06, 0x37, 0x64, 0x55, 0xC2, 0xF3, 
	0xA0, 0x91, 0x47, 0x76, 0x25, 0x14, 0x83, 0xB2, 0xE1, 0xD0, 
	0xFE, 0xCF, 0x9C, 0xAD, 0x3A, 0x0B, 0x58, 0x69, 0x04, 0x35,
	0x66, 0x57, 0xC0, 0xF1, 0xA2, 0x93, 0xBD, 0x8C, 0xDF, 0xEE, 
	0x79, 0x48, 0x1B, 0x2A, 0xC1, 0xF0, 0xA3, 0x92, 0x05, 0x34, 
	0x67, 0x56, 0x78, 0x49, 0x1A, 0x2B, 0xBC, 0x8D, 0xDE, 0xEF, 
	0x82, 0xB3, 0xE0, 0xD1, 0x46, 0x77, 0x24, 0x15, 0x3B, 0x0A, 
    0x59, 0x68, 0xFF, 0xCE, 0x9D, 0xAC
};

uint8_t CRC8(uint8_t *pBuffer,  uint32_t Length)
{
    uint8_t crc = 0x00;

    while(Length--)
    {
        crc = crc8_table[crc ^ *pBuffer++];
    }
    return (crc);
}

void MX25xx_Select(void)
{
    MX25xx_GP -> BSRR = MX25xx_CE << 16;
    __nop();__nop();__nop();__nop();__nop();
}

void MX25xx_Deselect(void)
{
    MX25xx_GP -> BSRR = MX25xx_CE;
    __nop();__nop();__nop();__nop();__nop();
}

void MX25xx_WriteEnable(void)
{
    MX25xx_Select();
    SPI_RW_Byte(SPI_mx25xx, CMD_Write_EN);
    MX25xx_Deselect();
}

void MX25xx_WriteDisable(void)   
{  
    MX25xx_Select();
    SPI_RW_Byte(SPI_mx25xx, CMD_Write_DIS);
    MX25xx_Deselect();  	      
} 

/*uint8_t MX25xx_ReadSR(void)
{
    uint8_t SR_value;
    MX25xx_Select();
    SPI_RW_Byte(SPI_mx25xx, CMD_Read_SR);
    SR_value = SPI_RW_Byte(SPI_mx25xx, Dummy_Byte); 
    MX25xx_Deselect();
    return SR_value;
}

uint8_t MX25xx_ReadCR(void)
{
    uint8_t CR_value;
    MX25xx_Select();
    SPI_RW_Byte(SPI_mx25xx, CMD_Read_CR);
    CR_value = SPI_RW_Byte(SPI_mx25xx, Dummy_Byte);
    MX25xx_Deselect();
    return CR_value;
}*/
 
 uint8_t MX25xx_ReadSR1(void)
{
    uint8_t SR_value;
    MX25xx_Select();
    SPI_RW_Byte(SPI_mx25xx, CMD_Read_SR1);
    SR_value = SPI_RW_Byte(SPI_mx25xx, Dummy_Byte);
    MX25xx_Deselect();
    return SR_value;
}

uint8_t MX25xx_ReadSR2(void)
{
    uint8_t SR_value;
    MX25xx_Select();
    SPI_RW_Byte(SPI_mx25xx, CMD_Read_SR2);
    SR_value = SPI_RW_Byte(SPI_mx25xx, Dummy_Byte);
    MX25xx_Deselect();
    return SR_value;
}
 
uint16_t MX25xx_ReadSR(void)
{
    return (((uint16_t)MX25xx_ReadSR1() << 8) | MX25xx_ReadSR2());
}

void MX25xx_WaitFree(void)
{
    MX25xx_Select();
    SPI_RW_Byte(SPI_mx25xx, CMD_Read_SR1);
    while (SPI_RW_Byte(SPI_mx25xx, Dummy_Byte) & 0x01){}
    MX25xx_Deselect();     
}
 
/*void MX25xx_WaitFree(void)
{
    MX25xx_Select();
    SPI_RW_Byte(SPI_mx25xx, CMD_Read_SR);
    while (SPI_RW_Byte(SPI_mx25xx, Dummy_Byte) & 0x01){}
    MX25xx_Deselect();     
}*/

void MX25xx_WriteSR(uint16_t SR_value)   
{  
    MX25xx_WriteEnable();
    MX25xx_Select();                 
    SPI_RW_Byte(SPI_mx25xx, CMD_Write_SR);
    SPI_RW_Byte(SPI_mx25xx, (uint8_t)(SR_value & 0x00FF));  
    SPI_RW_Byte(SPI_mx25xx, (uint8_t)(SR_value >> 8)); 
    MX25xx_Deselect(); 
    MX25xx_WaitFree();
} 

void MX25xx_ReadBuffer(uint8_t *pBuffer, uint32_t ReadAddr, uint32_t NumByteToRead)
{
    MX25xx_Select();   
    SPI_RW_Byte(SPI_mx25xx, CMD_Read_Data);
    //SPI_RW_Byte(SPI_mx25xx, CMD_Fast_Read);  // Fast Read 0Bh
    SPI_RW_Byte(SPI_mx25xx, (ReadAddr >> 16) & 0xFF);
    SPI_RW_Byte(SPI_mx25xx, (ReadAddr >> 8) & 0xFF);
    SPI_RW_Byte(SPI_mx25xx, (ReadAddr & 0xFF));
    //SPI_RW_Byte(SPI_mx25xx, Dummy_Byte);  // Fast Read 0Bh
    while( NumByteToRead-- ) 
    {
        *pBuffer++ = SPI_RW_Byte(SPI_mx25xx, Dummy_Byte);
    }
    MX25xx_Deselect();  
}

uint8_t MX25xx_ReadBufferWithCheck(uint8_t *pBuffer, uint32_t ReadAddr, uint32_t NumByteToRead)
{
    uint8_t  crc8 = 0, *pbuf = NULL;
    uint32_t idx = 0;
    
    pbuf = malloc(NumByteToRead);
    if(pbuf == NULL)
    {
        printf("malloc fail!\n");
        return 0;
    }
    
    MX25xx_ReadBuffer(pbuf, ReadAddr, NumByteToRead);
    crc8 = CRC8(pbuf, NumByteToRead);
    
    for(idx=0; idx<NumByteToRead; idx++)
    {
        *pBuffer++ = pbuf[idx];
    }
    
    if(pbuf)
    {
        free(pbuf);
        pbuf = NULL;
    }
    
    return crc8;
}

void MX25xx_PageProgram(uint8_t *pBuffer, uint32_t WriteAddr, uint32_t NumByteToWrite)
{
    MX25xx_WriteEnable();
    MX25xx_Select();
    SPI_RW_Byte(SPI_mx25xx, CMD_Page_Program);
    SPI_RW_Byte(SPI_mx25xx, (WriteAddr >> 16) & 0xFF);
    SPI_RW_Byte(SPI_mx25xx, (WriteAddr >> 8) & 0xFF);
    SPI_RW_Byte(SPI_mx25xx, (WriteAddr & 0xFF));

    if(NumByteToWrite > Page_Size)
    {
        NumByteToWrite = Page_Size;
    }

    while( NumByteToWrite-- )
    {
        SPI_RW_Byte(SPI_mx25xx, *pBuffer++);
    }

    MX25xx_Deselect();     
    MX25xx_WaitFree();
}

void MX25xx_WriteBuffer(uint8_t *pBuffer, uint32_t WriteAddr, uint32_t NumByteToWrite)
{
    uint8_t  NumOfSingle = 0, Addr = 0, count = 0, temp = 0;
    uint32_t NumOfPage = 0;
    
    Addr = WriteAddr % Page_Size;
    count = Page_Size - Addr;
    NumOfPage =  NumByteToWrite / Page_Size;
    NumOfSingle = NumByteToWrite % Page_Size;
    
    if (Addr == 0) // WriteAddr is aligned 
    {
        if (NumOfPage == 0)  // NumByteToWrite < Page_Size
        {
            MX25xx_PageProgram(pBuffer, WriteAddr, NumByteToWrite);
        }
        else  // NumByteToWrite >= Page_Size
        {
            while (NumOfPage--)
            {
                MX25xx_PageProgram(pBuffer, WriteAddr, Page_Size);
                WriteAddr += Page_Size;
                pBuffer += Page_Size;
            }

            MX25xx_PageProgram(pBuffer, WriteAddr, NumOfSingle);
        }
    }
    else // WriteAddr is not aligned 
    {
        if (NumOfPage == 0) // NumByteToWrite < Page_Size
        {
            if (NumOfSingle > count) // (NumByteToWrite + WriteAddr) > Page_Size
            {
                temp = NumOfSingle - count;

                MX25xx_PageProgram(pBuffer, WriteAddr, count);
                WriteAddr +=  count;
                pBuffer += count;

                MX25xx_PageProgram(pBuffer, WriteAddr, temp);
            }
            else
            {
                MX25xx_PageProgram(pBuffer, WriteAddr, NumByteToWrite);
            }
        }
        else // NumByteToWrite >= Page_Size
        {
            NumByteToWrite -= count;
            NumOfPage =  NumByteToWrite / Page_Size;
            NumOfSingle = NumByteToWrite % Page_Size;

            MX25xx_PageProgram(pBuffer, WriteAddr, count);
            WriteAddr +=  count;
            pBuffer += count;

            while (NumOfPage--)
            {
                MX25xx_PageProgram(pBuffer, WriteAddr, Page_Size);
                WriteAddr +=  Page_Size;
                pBuffer += Page_Size;
            }

            if (NumOfSingle != 0)
            {
                MX25xx_PageProgram(pBuffer, WriteAddr, NumOfSingle);
            }
        }
    }
}

uint8_t MX25xx_WriteBufferWithCheck(uint8_t *pBuffer, uint32_t WriteAddr, uint32_t NumByteToWrite, uint8_t crc8)
{
    uint8_t  crc_8 = 0, *pbuf = NULL;
    uint8_t  buf_4k[Sector_Size] = {0};
    uint32_t ser1 = 0, num1 = 0, lst1 = 0, ser2 = 0, num2 = 0, lst2 = 0, num = 0, idx1,idx2;
    
    ser1 = WriteAddr / Sector_Size;
    num1 = WriteAddr % Sector_Size;
    lst1 = Sector_Size - num1;
    ser2 = (WriteAddr + NumByteToWrite) / Sector_Size;
    num2 = (WriteAddr + NumByteToWrite) % Sector_Size;
    lst2 = Sector_Size - num2;
    num  = ser2 - ser1;
   
    if(num == 0)
    {
        /* 读出原始4k数据 */
        MX25xx_ReadBuffer(buf_4k, Sector_Size*ser1, Sector_Size);
        /* 擦除此节 */
        MX25xx_SectorErase(Sector_Size*ser1);
        /* 修改数据 */
        while(NumByteToWrite--)
        {
            buf_4k[num1++] = *pBuffer++;
        }
        /* 将含有修改的数据一次写入 */
        MX25xx_WriteBuffer(buf_4k, Sector_Size*ser1, Sector_Size);
    }
    else
    {
        /* 读出最前页原始4k数据 */
        MX25xx_ReadBuffer(buf_4k, Sector_Size*ser1, Sector_Size);
        /* 擦除此节 */
        MX25xx_SectorErase(Sector_Size*ser1);
        /* 修改数据 */
        while(lst1--)
        {
            buf_4k[num1++] = *pBuffer++;
        }
        /* 将含有修改的数据一次写入 */
        MX25xx_WriteBuffer(buf_4k, Sector_Size*ser1, Sector_Size);
        
        /* 中间页的操作 */
        for(idx1=1; idx1<num; idx1++)
        {
            /* 读出原始4k数据 */
            MX25xx_ReadBuffer(buf_4k, Sector_Size*(ser1+idx1), Sector_Size);
            /* 擦除此节 */
            MX25xx_SectorErase(Sector_Size*(ser1+idx1));
            /* 修改数据 */
            for(idx2=0; idx2<Sector_Size; idx2++)
            {
                buf_4k[idx2] = *pBuffer++;
            }
            /* 将含有修改的数据一次写入 */
            MX25xx_WriteBuffer(buf_4k, Sector_Size*(ser1+idx1), Sector_Size);
        }
        
        /* 读出最后页原始4k数据 */
        MX25xx_ReadBuffer(buf_4k, Sector_Size*ser2, Sector_Size);
        /* 擦除此节 */
        MX25xx_SectorErase(Sector_Size*ser2);
        /* 修改数据 */
        while(num2--)
        {
            buf_4k[Sector_Size-lst2-num2] = *pBuffer++;
        }
        /* 将含有修改的数据一次写入 */
        MX25xx_WriteBuffer(buf_4k, Sector_Size*ser2, Sector_Size);
    }
    
    /* 申请空间以作校验 */
    pbuf = malloc(NumByteToWrite);
    if(pbuf == NULL)
    {
        printf("malloc fail!\n");
        return 0;
    }
    /* 读出刚写入的数据 */
    MX25xx_ReadBuffer(pbuf, WriteAddr, NumByteToWrite);
    /* 获取校验值 */
    crc8 = CRC8(pbuf, NumByteToWrite);
    
    if(pbuf)
    {
        free(pbuf);
        pbuf = NULL;
    }
    
    if(crc_8 == crc8)
        return 1;
    else
        return 2;
}

void MX25xx_SectorErase(uint32_t SectorAddr) // 4kB
{
    MX25xx_WriteEnable();
    MX25xx_WaitFree();
    MX25xx_Select();   
    SPI_RW_Byte(SPI_mx25xx, CMD_Sector_Erase);
    SPI_RW_Byte(SPI_mx25xx, (SectorAddr >> 16) & 0xFF);
    SPI_RW_Byte(SPI_mx25xx, (SectorAddr >> 8) & 0xFF);
    SPI_RW_Byte(SPI_mx25xx, (SectorAddr & 0xFF));
    MX25xx_Deselect(); 
    MX25xx_WaitFree();
}

void MX25xx_BlockErase32(uint32_t BlockAddr) 
{
    MX25xx_WriteEnable();
    MX25xx_Select();   
    SPI_RW_Byte(SPI_mx25xx, CMD_Block_Erase32);
    SPI_RW_Byte(SPI_mx25xx, (BlockAddr >> 16) & 0xFF);
    SPI_RW_Byte(SPI_mx25xx, (BlockAddr >> 8) & 0xFF);
    SPI_RW_Byte(SPI_mx25xx, (BlockAddr & 0xFF));
    MX25xx_Deselect(); 
    MX25xx_WaitFree();
}

void MX25xx_BlockErase64(uint32_t BlockAddr) 
{
    MX25xx_WriteEnable();
    MX25xx_Select();   
    SPI_RW_Byte(SPI_mx25xx, CMD_Block_Erase64);
    SPI_RW_Byte(SPI_mx25xx, (BlockAddr >> 16) & 0xFF);
    SPI_RW_Byte(SPI_mx25xx, (BlockAddr >> 8) & 0xFF);
    SPI_RW_Byte(SPI_mx25xx, (BlockAddr & 0xFF));
    MX25xx_Deselect(); 
    MX25xx_WaitFree();
}

void MX25xx_ChipErase(void) 
{
    MX25xx_WriteEnable();
    MX25xx_Select();   
    SPI_RW_Byte(SPI_mx25xx, CMD_Chip_Erase);
    MX25xx_Deselect(); 
    MX25xx_WaitFree();
}

void MX25xx_PowerDown(void)  //lower power
{
    uint8_t delay = 0;
    MX25xx_Select();   
    SPI_RW_Byte(SPI_mx25xx, CMD_Power_Down);
    MX25xx_Deselect();   
    while(delay++ < 24);   //5us	
}

void MX25xx_ReleasePowerDown(void)
{
    uint8_t delay = 0;
    MX25xx_Select();   
    SPI_RW_Byte(SPI_mx25xx, CMD_Release_HPM);
    MX25xx_Deselect(); 
    while(delay++ < 24);   //5us	 
}

uint16_t MX25xx_ReadDevice_ID(void)
{
    uint16_t temp0 = 0, temp1 = 0;
    MX25xx_Select();   
    SPI_RW_Byte(SPI_mx25xx, CMD_Device_ID);
    SPI_RW_Byte(SPI_mx25xx, 0x00);
    SPI_RW_Byte(SPI_mx25xx, 0x00);
    SPI_RW_Byte(SPI_mx25xx, 0x00);
    temp0 = SPI_RW_Byte(SPI_mx25xx, Dummy_Byte);
    temp1 = SPI_RW_Byte(SPI_mx25xx, Dummy_Byte);
    MX25xx_Deselect();  
    printf("Manufacturer ID is %x!\nDevice ID is %x!\n", temp0, temp1);
    temp0 = (temp0 << 8) | temp1;
    return temp0;
}

uint64_t MX25xx_ReadUnique_ID(void)
{
    uint32_t ID64 = 0, i = 0;
    MX25xx_Select();
    SPI_RW_Byte(SPI_mx25xx, CMD_Unique_ID);
    while(i++ < 12)
    {
        if(i < 5)
        {
            SPI_RW_Byte(SPI_mx25xx, Dummy_Byte);
        }
        else
        {
            ID64 = (ID64 << 8) | SPI_RW_Byte(SPI_mx25xx, Dummy_Byte);
        }
    }
    MX25xx_Deselect();  
    printf("Unique ID is %x!\n", ID64);
    return ID64;
}

uint32_t MX25xx_ReadJEDEC_ID(void)
{
    uint32_t temp = 0, temp0 = 0, temp1 = 0, temp2 = 0;
    MX25xx_Select();
    SPI_RW_Byte(SPI_mx25xx, CMD_JEDEC_ID);
    temp0 = SPI_RW_Byte(SPI_mx25xx, Dummy_Byte);
    temp1 = SPI_RW_Byte(SPI_mx25xx, Dummy_Byte);
    temp2 = SPI_RW_Byte(SPI_mx25xx, Dummy_Byte);
    MX25xx_Deselect();  
    printf("Manufacturer ID is %x!\nMemory Type ID is %x!\nCapacity ID is %x!\n", temp0, temp1, temp2);
    temp = (temp0 << 16) | (temp1 << 8) | temp2;
    return temp;
}
