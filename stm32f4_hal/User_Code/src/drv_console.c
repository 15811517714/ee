#include "drv_console.h"

#ifdef __GNUC__
    #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
    #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif 

PUTCHAR_PROTOTYPE
{
    USART1->SR;
    USART1->DR = (unsigned char)ch;   
    while(!(USART1->SR & 0x80));
    return 0;
}

void Console_Init(void)
{
    GPIO_InitTypeDef     GPIO_Initure;
    USART_HandleTypeDef  USART1_Initure;
    
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_USART1_CLK_ENABLE();
    
    GPIO_Initure.Pin = GPIO_PIN_9 | GPIO_PIN_10;
    GPIO_Initure.Mode = GPIO_MODE_AF_PP;
    GPIO_Initure.Pull = GPIO_NOPULL;
    GPIO_Initure.Speed = GPIO_SPEED_FAST;
    GPIO_Initure.Alternate = GPIO_AF7_USART1;
    HAL_GPIO_Init(GPIOA, &GPIO_Initure);
    
    USART1_Initure.Instance = USART1;
	USART1_Initure.Init.BaudRate = 115200;
	USART1_Initure.Init.WordLength = USART_WORDLENGTH_8B;
	USART1_Initure.Init.StopBits = USART_STOPBITS_1;
	USART1_Initure.Init.Parity = USART_PARITY_NONE;
	USART1_Initure.Init.Mode = USART_MODE_TX_RX;
	HAL_USART_Init(&USART1_Initure);
}
