#ifndef __AT_H
#define __AT_H

#include "stm32f4xx.h"

#ifdef __cplusplus
extern "C" {
#endif

#define AT_CMD_MAX_LEN                 1024     /* 字符格式发送,所用长度翻倍 */   
#define AT_CLIENT_NUM_MAX              1        /* 最大同时支持的AT客户端数量             */
#define AT_CLIENT_REVICE_MAX           1536     /* AT组件客户端单次接收的最大长度         */
#define AT_PRINT_RAW_CMD                        /* 用于开启 AT 命令通信数据的实时回显     */

#define AT_CMD_EXPORT(_name_, _args_expr_, _test_, _query_, _setup_, _exec_)   \
    RT_USED static const struct at_cmd __at_cmd_##_test_##_query_##_setup_##_exec_ SECTION("RtAtCmdTab") = \
    {                                                                          \
        _name_,                                                                \
        _args_expr_,                                                           \
        _test_,                                                                \
        _query_,                                                               \
        _setup_,                                                               \
        _exec_,                                                                \
    };
/******************************************************************************/
/* 单个客户端和多个客户端时兼容                                              */
/******************************************************************************/
#define at_exec_cmd(resp, ...)                   at_obj_exec_cmd(at_client_get_first(), resp, __VA_ARGS__)
#define at_client_wait_connect(timeout)          at_client_obj_wait_connect(at_client_get_first(), timeout)
#define at_client_send(buf, size)                at_client_obj_send(at_client_get_first(), buf, size)
#define at_client_recv(buf, size)                at_client_obj_recv(at_client_get_first(), buf, size)
#define at_set_end_sign(ch)                      at_obj_set_end_sign(at_client_get_first(), ch)
#define at_set_urc_table(urc_table, table_sz)    at_obj_set_urc_table(at_client_get_first(), urc_table, table_sz)


/******************************************************************************/
/*                              全局数据类型定义                              */
/******************************************************************************/
enum at_status
{
    AT_STATUS_UNINITIALIZED = 0,        /* 未初始化状态                       */
    AT_STATUS_INITIALIZED,              /* 初始化状态                         */
    AT_STATUS_BUSY,                     /* 忙碌状态                           */
};
typedef enum at_status at_status_t;

enum at_resp_status
{
     AT_RESP_OK = 0,                   /* AT指令响应是： OK                   */
     AT_RESP_ERROR = -1,               /* AT响应数据是：ERROR                 */
     AT_RESP_TIMEOUT = -2,             /* AT响应超时时间                      */
     AT_RESP_BUFF_FULL = -3,           /* AT响应满                            */
};
typedef enum at_resp_status at_resp_status_t;

struct at_response      /* 响应数据结构体                                     */
{
    char *buf;          /* 存放接收到的响应数据，去除结束符（"\r\n"）的数据   */
                        /* 此变量只是个指向字符数组的32位的地址数值,只占4字节 */
                        /* 而不是一个大的数组空间.所以总的 sizeof(struct at_respone) */
                        /* 所占的空间就只有 4个int + char* 类型的大小, 共计20B*/
    int buf_size;       /* 预设的最大响应长度                                 */
    int line_num;       /* 本次响应数据需要接收的行数，如果没有响应行数限定需求，可以置为 0 */
                        /* == 0:当接收到“OK”或“ERROR”时，响应数据将自动返回   */
                        /* != 0:当接收的总行数等于设置的行数时，响应数据将返回*/
    int line_counts;    /* 记录本次响应数据实际接收到的总行数              */
    int timeout;        /* 预设的最大响应等待时间                             */
};
typedef struct at_response *at_response_t;

struct at_urc                                        /* URC数据结构体         */
{
    const char *cmd_prefix;                          /* 前缀                  */
    const char *cmd_suffix;                          /* 后缀                  */
    void (*func)(const char *data, int size);        /* 回调函数              */
};
typedef struct at_urc *at_urc_t;

struct at_order            /* 主动下发的AT指令结构体                          */
{
    const char *cmd_at;    /* 需要发送的AT指令                                */
    int  buff_size;        /* 本次响应最大支持的接收数据的长度                */
    int  line_num;         /* 有效数据在响应数据中的行数值.若为0:则当接收到   */
                           /* "OK"或"ERROR"时停止响应的接收(会在超时时间内死  */
                           /* 等OK/ERROR);若大于0,则接收到指定行数时停止接收  */
    int timeout;           /* 该命令响应等待超时时间                          */
    char anew_times;       /* 允许重发次数                                    */
    
    int (*func)(at_response_t *iresp,int iline_num);
                           /* 数据响应回调函数,由成员line_num决定对应的2种方式*/
                           /* 第一种回传整个resp在内遍历，第二种提供返回的总行数，按行数索引有用返回*/
    void (*funb)(void);    /* 发送超时回调函数,超时时间是第4个参数timeout     */
};
typedef struct at_order *at_order_t;

struct at_client
{
    rt_device_t device;               /* 对应设备驱动的操作句柄               */

    at_status_t status;               /* AT 模块状态标志位   枚举型           */
    char end_sign;                    /* 结束标志                             */

    char *recv_buffer;                /* 接收缓存数据地址                     */
    int recv_bufsz;             /* 接收数据上限                         */
    int cur_recv_len;           /* 当前已接受长度                       */
    
    rt_sem_t rx_notice;               /* 接收信号量                           */
    rt_mutex_t lock;                  /* 互斥锁                               */

    at_response_t resp;               /* 响应数据结构体                       */
    rt_sem_t resp_notice;             /* 响应信号量                           */
    at_resp_status_t resp_status;     /* 响应结果信息：成功 错误 超时 失败    */

    const struct at_urc *urc_table;   /* URC表地址(未经请求的AT指令)          */
    int urc_table_size;         /* URC表成员数(未经请求数据表的大小)    */
    
    rt_thread_t parser;               /* 所用线程在系统中句柄                 */
};
typedef struct at_client *at_client_t;

/******************************************************************************/
/*                                全局变量声明                                */
/******************************************************************************/

/******************************************************************************/
/*                                 全局函数原型                               */
/******************************************************************************/
int at_client_init(const char *dev_name,  int recv_bufsz);
at_client_t at_client_get(const char *dev_name);
at_client_t at_client_get_first(void);
int at_client_obj_wait_connect(at_client_t client, int timeout);
int at_client_obj_send(at_client_t client, const char *buf, int size);
int at_client_obj_recv(at_client_t client, char *buf, int size);
void at_obj_set_end_sign(at_client_t client, char ch);
void at_obj_set_urc_table(at_client_t client, const struct at_urc * table, int size);
int at_obj_exec_cmd(at_client_t client, at_response_t resp, const char *cmd_expr, ...);
at_response_t at_create_resp(int buf_size, int line_num, int timeout);
void at_delete_resp(at_response_t resp);
at_response_t at_resp_set_info(at_response_t resp, int buf_size, int line_num, int timeout);
const char *at_resp_get_line(at_response_t resp, int resp_line);
const char *at_resp_get_line_by_kw(at_response_t resp, const char *keyword);
int at_resp_parse_line_args(at_response_t resp, int resp_line, const char *resp_expr, ...);
int at_resp_parse_line_args_by_kw(at_response_t resp, const char *keyword, const char *resp_expr, ...);

#ifdef __cplusplus
    }
#endif

#endif


