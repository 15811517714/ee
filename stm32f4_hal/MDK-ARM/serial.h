#ifndef __SERIAL_H__
#define __SERIAL_H__

#include "stm32f4xx.h"

/* 环形接受区 */
typedef struct
{
    unsigned char  *pbuf;
    unsigned int    put_char;
    unsigned int    get_char;
}serial_rx_fifo;

typedef struct
{
    void   *privat;
    serial_ops  ops;
    
}serial_device;

typedef struct
{
    int (*putc)(serial_device *serial, unsigned char c);
    int (*getc)(serial_device *serial);
}serial_ops;



#endif 