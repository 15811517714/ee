#include "serial.h"
#include <stdio.h>

static __inline int _serial_poll_rx(struct rt_serial_device *serial, unsigned char *data, int length)
{
    int ch;
    int size;
    
    assert_param(serial != NULL);
    size = length;

    while (length)
    {
        ch = serial->ops->getc(serial);
        *data = ch; 
        data ++; length --;

        if (ch == '\n') break;  /*  数据中如有\n, 则终止接收  */
    }

    return size - length;
}

static __inline int _serial_poll_tx(struct rt_serial_device *serial, const unsigned char *data, int length)
{
    int size;
    assert_param(serial != NULL);

    size = length;
    while (length)
    {

    
        serial->ops->putc(serial, *data);
    
        ++ data;
        -- length;
    }

    return size - length;
}

/*
 * Serial interrupt routines
 */
static __inline int _serial_int_rx(struct rt_serial_device *serial, unsigned char *data, int length)
{
    int size;
    struct rt_serial_rx_fifo* rx_fifo;

    assert_param(serial != NULL);
    size = length; 
    
    rx_fifo = (struct rt_serial_rx_fifo*) serial->serial_rx;
    assert_param(rx_fifo != NULL);

    /* read from software FIFO */
    while (length)
    {
        int ch;
        rt_base_t level;

        /* disable interrupt */
        level = rt_hw_interrupt_disable();
        if (rx_fifo->get_index != rx_fifo->put_index)
        {
            ch = rx_fifo->buffer[rx_fifo->get_index];
            rx_fifo->get_index += 1;
            if (rx_fifo->get_index >= serial->config.bufsz) 
                rx_fifo->get_index = 0;
        }
        else  /*  收入的全读出来后，读写标签就会相等,即无新数据  */
        {
            /* no data, enable interrupt and break out */
            rt_hw_interrupt_enable(level);
            break;
        }

        /* enable interrupt */
        rt_hw_interrupt_enable(level);

        *data = ch & 0xff;
        data ++; length --;
    }

    return size - length;
}

rt_inline int _serial_int_tx(struct rt_serial_device *serial, const unsigned char *data, int length)
{
    int size;
    struct rt_serial_tx_fifo *tx;
    
    assert_param(serial != NULL);

    size = length;
    tx = (struct rt_serial_tx_fifo*) serial->serial_tx;
    assert_param(tx != NULL);

    while (length)
    {
        if (serial->ops->putc(serial, *(char*)data) == -1)
        {
            rt_completion_wait(&(tx->completion), RT_WAITING_FOREVER);
            continue;
        }

        data ++; length --;
    }

    return size - length;
}

/*
 * Serial DMA routines
 */
rt_inline int _serial_dma_rx(struct rt_serial_device *serial, unsigned char *data, int length)
{
    rt_base_t level;
    int result = RT_EOK;
    struct rt_serial_rx_dma *rx_dma;

    assert_param((serial != NULL) && (data != NULL));
    rx_dma = (struct rt_serial_rx_dma*)serial->serial_rx;
    assert_param(rx_dma != NULL);

    level = rt_hw_interrupt_disable();
    if (rx_dma->activated != RT_TRUE)
    {
        rx_dma->activated = RT_TRUE;
        serial->ops->dma_transmit(serial, data, length, RT_SERIAL_DMA_RX);
    }
    else 
			result = -RT_EBUSY;
		
    rt_hw_interrupt_enable(level);

    if (result == RT_EOK) return length;

    rt_set_errno(result);
    return 0;
}

rt_inline int _serial_dma_tx(struct rt_serial_device *serial, const unsigned char *data, int length)
{
    rt_base_t level;
    rt_err_t result;
    struct rt_serial_tx_dma *tx_dma;

    tx_dma = (struct rt_serial_tx_dma*)(serial->serial_tx);
    
    result = rt_data_queue_push(&(tx_dma->data_queue), data, length, RT_WAITING_FOREVER); 
    if (result == RT_EOK)
    {
        level = rt_hw_interrupt_disable();
        if (tx_dma->activated != RT_TRUE)
        {
            tx_dma->activated = RT_TRUE;
            rt_hw_interrupt_enable(level);

            /* make a DMA transfer */
            serial->ops->dma_transmit(serial, data, length, RT_SERIAL_DMA_TX);
        }
        else
        {
            rt_hw_interrupt_enable(level);
        }

        return length;
    }
    else
    {
        rt_set_errno(result);
        return 0;
    }
}

/* RT-Thread Device Interface */
/*
 * This function initializes serial device.
 */
static rt_err_t rt_serial_init(struct rt_device *dev)
{
    rt_err_t result = RT_EOK;
    struct rt_serial_device *serial;

    assert_param(dev != NULL);
    serial = (struct rt_serial_device *)dev;

    /* initialize rx/tx */
    serial->serial_rx = NULL;
    serial->serial_tx = NULL;

    /* apply configuration */
    if (serial->ops->configure)
        result = serial->ops->configure(serial, &serial->config);

    return result;
}

static rt_err_t rt_serial_open(struct rt_device *dev, rt_uint16_t oflag)
{
    struct rt_serial_device *serial;

    assert_param(dev != NULL);
    serial = (struct rt_serial_device *)dev;

    /* check device flag with the open flag */
    if ((oflag & RT_DEVICE_FLAG_DMA_RX) && !(dev->flag & RT_DEVICE_FLAG_DMA_RX)) 
        return -RT_EIO;
    if ((oflag & RT_DEVICE_FLAG_DMA_TX) && !(dev->flag & RT_DEVICE_FLAG_DMA_TX))
        return -RT_EIO;
    if ((oflag & RT_DEVICE_FLAG_INT_RX) && !(dev->flag & RT_DEVICE_FLAG_INT_RX))
        return -RT_EIO;
    if ((oflag & RT_DEVICE_FLAG_INT_TX) && !(dev->flag & RT_DEVICE_FLAG_INT_TX))
        return -RT_EIO;

    /* get open flags */
    dev->open_flag = oflag & 0xff;
    
    /* initialize the Rx/Tx structure according to open flag */
    if (serial->serial_rx == NULL)
    {
        if (oflag & RT_DEVICE_FLAG_DMA_RX)
        {
            struct rt_serial_rx_dma* rx_dma;

            rx_dma = (struct rt_serial_rx_dma*) rt_malloc (sizeof(struct rt_serial_rx_dma));
            assert_param(rx_dma != NULL);
            rx_dma->activated = RT_FALSE;

            serial->serial_rx = rx_dma;
            dev->open_flag |= RT_DEVICE_FLAG_DMA_RX;
        }
        else if (oflag & RT_DEVICE_FLAG_INT_RX)
        {
            struct rt_serial_rx_fifo* rx_fifo;

            rx_fifo = (struct rt_serial_rx_fifo*) rt_malloc (sizeof(struct rt_serial_rx_fifo) + serial->config.bufsz);
            assert_param(rx_fifo != NULL);
            rx_fifo->buffer = (unsigned char*) (rx_fifo + 1);  /* 1 == sizeof(struct rt_serial_rx_fifo) */
            rt_memset(rx_fifo->buffer, 0, serial->config.bufsz);  //
            rx_fifo->put_index = 0;
            rx_fifo->get_index = 0;

            serial->serial_rx = rx_fifo;
            dev->open_flag |= RT_DEVICE_FLAG_INT_RX;
            /* configure low level device */
            serial->ops->control(serial, RT_DEVICE_CTRL_SET_INT, (void *)RT_DEVICE_FLAG_INT_RX);
        }
        else
        {
            serial->serial_rx = NULL;
        }
    }

    if (serial->serial_tx == NULL)
    {
        if (oflag & RT_DEVICE_FLAG_DMA_TX)
        {
            struct rt_serial_tx_dma* tx_dma;

            tx_dma = (struct rt_serial_tx_dma*) rt_malloc (sizeof(struct rt_serial_tx_dma));
            assert_param(tx_dma != NULL);
            
            rt_data_queue_init(&(tx_dma->data_queue), 8, 4, NULL);
            serial->serial_tx = tx_dma;

            dev->open_flag |= RT_DEVICE_FLAG_DMA_TX;
        }
        else if (oflag & RT_DEVICE_FLAG_INT_TX)
        {
            struct rt_serial_tx_fifo *tx_fifo;

            tx_fifo = (struct rt_serial_tx_fifo*) rt_malloc(sizeof(struct rt_serial_tx_fifo));
            assert_param(tx_fifo != NULL);

            rt_completion_init(&(tx_fifo->completion));
            serial->serial_tx = tx_fifo;

            dev->open_flag |= RT_DEVICE_FLAG_INT_TX;
            /* configure low level device */
            serial->ops->control(serial, RT_DEVICE_CTRL_SET_INT, (void *)RT_DEVICE_FLAG_INT_TX);
        }
        else
        {
            serial->serial_tx = NULL;
        }
    }

    return RT_EOK;
}

static rt_err_t rt_serial_close(struct rt_device *dev)
{
    struct rt_serial_device *serial;

    assert_param(dev != NULL);
    serial = (struct rt_serial_device *)dev;

    /* this device has more reference count */
    if (dev->ref_count > 1) return RT_EOK;
    
    if (dev->open_flag & RT_DEVICE_FLAG_INT_RX)
    {
        struct rt_serial_rx_fifo* rx_fifo;

        rx_fifo = (struct rt_serial_rx_fifo*)serial->serial_rx;
        assert_param(rx_fifo != NULL);

        rt_free(rx_fifo);
        serial->serial_rx = NULL;
        dev->open_flag &= ~RT_DEVICE_FLAG_INT_RX;
        /* configure low level device */
        serial->ops->control(serial, RT_DEVICE_CTRL_CLR_INT, (void*)RT_DEVICE_FLAG_INT_TX);
    }
    else if (dev->open_flag & RT_DEVICE_FLAG_DMA_RX)
    {
        struct rt_serial_rx_dma* rx_dma;

        rx_dma = (struct rt_serial_rx_dma*)serial->serial_tx;
        assert_param(rx_dma != NULL);

        rt_free(rx_dma);
        serial->serial_rx = NULL;
        dev->open_flag &= ~RT_DEVICE_FLAG_DMA_RX;
    }

    if (dev->open_flag & RT_DEVICE_FLAG_INT_TX)
    {
        struct rt_serial_tx_fifo* tx_fifo;

        tx_fifo = (struct rt_serial_tx_fifo*)serial->serial_rx;
        assert_param(tx_fifo != NULL);

        rt_free(tx_fifo);
        serial->serial_tx = NULL;
        dev->open_flag &= ~RT_DEVICE_FLAG_INT_TX;
        /* configure low level device */
        serial->ops->control(serial, RT_DEVICE_CTRL_CLR_INT, (void*)RT_DEVICE_FLAG_INT_TX);
    }
    else if (dev->open_flag & RT_DEVICE_FLAG_DMA_TX)
    {
        struct rt_serial_tx_dma* tx_dma;

        tx_dma = (struct rt_serial_tx_dma*)serial->serial_tx;
        assert_param(tx_dma != NULL);

        rt_free(tx_dma);
        serial->serial_tx = NULL;
        dev->open_flag &= ~RT_DEVICE_FLAG_DMA_TX;
    }

    return RT_EOK;
}

static rt_size_t rt_serial_read(struct rt_device *dev,
                                rt_off_t          pos,
                                void             *buffer,
                                rt_size_t         size)
{
    struct rt_serial_device *serial;

    assert_param(dev != NULL);
    if (size == 0) return 0;

    serial = (struct rt_serial_device *)dev;

    if (dev->open_flag & RT_DEVICE_FLAG_INT_RX)  /* dev->open_flag 等同于 serial->parent->open_flag */
    {
        return _serial_int_rx(serial, buffer, size);
    }
    else if (dev->open_flag & RT_DEVICE_FLAG_DMA_RX)
    {
        return _serial_dma_rx(serial, buffer, size);
    }

    return _serial_poll_rx(serial, buffer, size);
}

static rt_size_t rt_serial_write(struct rt_device *dev,
                                 rt_off_t          pos,
                                 const void       *buffer,
                                 rt_size_t         size)
{
    struct rt_serial_device *serial;

    assert_param(dev != NULL);
    if (size == 0) return 0;

    serial = (struct rt_serial_device *)dev;

    if (dev->open_flag & RT_DEVICE_FLAG_INT_TX)
    {
        return _serial_int_tx(serial, buffer, size);
    }
    else if (dev->open_flag & RT_DEVICE_FLAG_DMA_TX)
    {
        return _serial_dma_tx(serial, buffer, size);
    }
    else
    {
        return _serial_poll_tx(serial, buffer, size);
    }
}

static rt_err_t rt_serial_control(struct rt_device *dev,
                                  unsigned char        cmd,
                                  void             *args)
{
    struct rt_serial_device *serial;

    assert_param(dev != NULL);
    serial = (struct rt_serial_device *)dev;

    switch (cmd)
    {
        case RT_DEVICE_CTRL_SUSPEND:
            /* suspend device */
            dev->flag |= RT_DEVICE_FLAG_SUSPENDED;
            break;

        case RT_DEVICE_CTRL_RESUME:
            /* resume device */
            dev->flag &= ~RT_DEVICE_FLAG_SUSPENDED;
            break;

        case RT_DEVICE_CTRL_CONFIG:
            /* configure device */
            serial->ops->configure(serial, (struct serial_configure *)args);
            break;

        default :
            /* control device */
            serial->ops->control(serial, cmd, args);
            break;
    }

    return RT_EOK;
}

/*
 * serial register
 */
rt_err_t rt_hw_serial_register(struct rt_serial_device *serial,
                               const char              *name,
                               rt_uint32_t              flag,
                               void                    *data)
{
    struct rt_device *device;
    assert_param(serial != NULL);

    device = &(serial->parent);

    device->type        = RT_Device_Class_Char;
    device->rx_indicate = NULL;
    device->tx_complete = NULL;

    device->init        = rt_serial_init;
    device->open        = rt_serial_open;
    device->close       = rt_serial_close;
    device->read        = rt_serial_read;
    device->write       = rt_serial_write;
    device->control     = rt_serial_control;
    device->user_data   = data;

    /* register a character device */
    return rt_device_register(device, name, flag);
}

void hal_serial_isr(struct rt_serial_device *serial, int event)
{
    switch (event & 0xff)
    {
        case RT_SERIAL_EVENT_RX_IND:
        {
            int ch = -1;
            rt_base_t level;
            struct rt_serial_rx_fifo* rx_fifo;
            
            /* 局部变量rx_fifo绑定到全局量serial，之后收入到rx_fifo的数据就收到了全局量serial的接收区serial_rx */
            rx_fifo = (struct rt_serial_rx_fifo*)serial->serial_rx; 
            assert_param(rx_fifo != NULL);
            
            /* 中断接收模式 */
            assert_param(serial->parent.open_flag & RT_DEVICE_FLAG_INT_RX);
            
            /* 接收到第一个字节后就会初始完rx_fifo,之后进入while(1),在getc中直到收到-1认为接收完毕,类似IDLE的方式实现 */
            while (1)
            {
                ch = serial->ops->getc(serial);
                if (ch == -1) break;
                /* disable interrupt */
                level = rt_hw_interrupt_disable();
                
                rx_fifo->buffer[rx_fifo->put_index] = ch;
                rx_fifo->put_index += 1;

                if (rx_fifo->put_index >= serial->config.bufsz) 
                    rx_fifo->put_index = 0;
                
                /* if the next position is read index, discard this 'read char' */
                if (rx_fifo->put_index == rx_fifo->get_index)  /* put_index是放入标签，get_index是读出标签 */
                {
                    rx_fifo->get_index += 1;
                    if (rx_fifo->get_index >= serial->config.bufsz) 
                        rx_fifo->get_index = 0;
                }
                
                /* enable interrupt */
                rt_hw_interrupt_enable(level);
            }
            
            /* invoke callback */
            if (serial->parent.rx_indicate != NULL) /* 连续的数据流断了之后就会触发回调函数,返回此次接受的设备和接收总长,相当于IDLE中断 */
            {
                rt_size_t rx_length;
            
                /* get rx length */
                level = rt_hw_interrupt_disable();
                rx_length = (rx_fifo->put_index >= rx_fifo->get_index)?(rx_fifo->put_index - rx_fifo->get_index):(serial->config.bufsz - (rx_fifo->get_index - rx_fifo->put_index)); 
                rt_hw_interrupt_enable(level);

                serial->parent.rx_indicate(&serial->parent, rx_length);
            }
            break;
        }
        case RT_SERIAL_EVENT_TX_DONE:
        {
            struct rt_serial_tx_fifo* tx_fifo;

            tx_fifo = (struct rt_serial_tx_fifo*)serial->serial_tx;
            rt_completion_done(&(tx_fifo->completion));
            break;
        }
        case RT_SERIAL_EVENT_TX_DMADONE:
        {
            const void *data_ptr;
            rt_size_t data_size;
            const void *last_data_ptr;
            struct rt_serial_tx_dma* tx_dma;

            tx_dma = (struct rt_serial_tx_dma*) serial->serial_tx;
            
            rt_data_queue_pop(&(tx_dma->data_queue), &last_data_ptr, &data_size, 0);
            if (rt_data_queue_peak(&(tx_dma->data_queue), &data_ptr, &data_size) == RT_EOK)
            {
                /* transmit next data node */
                tx_dma->activated = RT_TRUE;
                serial->ops->dma_transmit(serial, data_ptr, data_size, RT_SERIAL_DMA_TX);
            }
            else
            {
                tx_dma->activated = RT_FALSE;
            }
            
            /* invoke callback */
            if (serial->parent.tx_complete != NULL)
            {
                serial->parent.tx_complete(&serial->parent, (void*)last_data_ptr);
            }
            break;
        }
        case RT_SERIAL_EVENT_RX_DMADONE:
        {
            int length;
            struct rt_serial_rx_dma* rx_dma;

            rx_dma = (struct rt_serial_rx_dma*)serial->serial_rx;
            /* get DMA rx length */
            length = (event & (~0xff)) >> 8;
            serial->parent.rx_indicate(&(serial->parent), length);
            rx_dma->activated = RT_FALSE;
            break;
        }
    }
}
