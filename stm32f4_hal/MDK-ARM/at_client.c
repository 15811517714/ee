#include <at.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define AT_RESP_END_OK      "OK"
#define AT_RESP_END_ERROR   "ERROR"
#define AT_RESP_END_FAIL    "FAIL"
#define AT_END_CR_LF        "\r\n"

static struct at_client at_client_table[AT_CLIENT_NUM_MAX] = { 0 };/* 设备列表*/

extern rt_size_t at_vprintfln(rt_device_t device, const char *format, va_list args);
extern void at_print_raw_cmd(const char *type, const char *cmd, rt_size_t size);
extern const char *at_get_last_cmd(rt_size_t *cmd_size);
extern bool HTTP_FLAG;   /* 判断是否处于HTTP的模式下 */ 

/*<FUNC+>**********************************************************************/
/* 函数名称: at_create_resp                                                   */
/* 功能描述: 创建AT命令的响应结构体(实际响应的各行数据是以0D00结尾的,而非0D0A)*/
/* 输入参数: buf_size --- 最大响应缓冲区大小                                  */
/*           line_num --- 设置响应行数                                        */
/*           timeout --- 超时最大响应时间                                     */
/* 输出参数: 无                                                               */
/* 返 回 值: != RT_NULL: 响应结构体   = RT_NULL 错误                          */
/* 操作流程:                                                                  */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-01-12              1             创建响应    创建函数             */
/*<FUNC->**********************************************************************/
at_response_t at_create_resp(rt_size_t buf_size, rt_size_t line_num, rt_int32_t timeout)
{
    at_response_t resp = RT_NULL;

    resp = (at_response_t) rt_calloc(1, sizeof(struct at_response));
    if (resp == RT_NULL)
    {
        LOG_ERR("AT create response object failed! No memory for response object!");
        return RT_NULL;
    }

    resp->buf = (char *) rt_calloc(1, buf_size);
    if (resp->buf == RT_NULL)
    {
        LOG_ERR("AT create response object failed! No memory for response buffer!");
        rt_free(resp);
        return RT_NULL;
    }

    resp->buf_size = buf_size;
    resp->line_num = line_num;
    resp->line_counts = 0;
    resp->timeout = timeout;

    return resp;
}

/*<FUNC+>**********************************************************************/
/* 函数名称: at_delete_resp                                                   */
/* 功能描述: 释放结构体指针空间                                               */
/* 输入参数: resp --- 结构体句柄                                              */
/* 输出参数: 无                                                               */
/* 返 回 值: void                                                             */
/* 操作流程:                                                                  */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-04-2               1             创建响?    创建函数              */
/*<FUNC->**********************************************************************/
void at_delete_resp(at_response_t resp)
{
    if (resp && resp->buf)
    {
        rt_free(resp->buf);
    }

    if (resp)
    {
        rt_free(resp);
        resp = RT_NULL;
    }
}
/*<FUNC+>**********************************************************************/
/* 函数名称: at_resp_set_info                                                 */
/* 功能描述: 重新调整已创建的响应结构体的参数函数                             */
/* 输入参数: resp ---已经创建的响应结构体指针                                 */
/*           buf_size --- 本次响应最大支持的接收数据的长度                    */
/*           line_num --- 本次响应需要返回数据的行数                          */
/*           timeout --- 本次响应数据最大响应时间，数据接收超时返回错误       */
/* 输出参数: 无                                                               */
/* 返 回 值: != NULL 成功，返回指向响应结构体的指针；  = NULL 失败，内存不足  */
/* 操作流程:                                                                  */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-03-31              1             创建响?    创建函数              */
/*<FUNC->**********************************************************************/
at_response_t at_resp_set_info(at_response_t resp, rt_size_t buf_size, rt_size_t line_num, rt_int32_t timeout)
{
    RT_ASSERT(resp);

    if (resp->buf_size != buf_size)
    {
        resp->buf_size = buf_size;

        resp->buf = (char *) rt_realloc(resp->buf, buf_size);
        if (!resp->buf)
        {
            LOG_INFO("No memory for realloc response buffer size(%d).", buf_size);
            return RT_NULL;
        }
    }

    resp->line_num = line_num;
    resp->timeout = timeout;

    return resp;
}
/*<FUNC+>**********************************************************************/
/* 函数名称: at_resp_get_line                                                 */
/* 功能描述: 获取指定行号得响应数据                                           */
/* 输入参数: resp --- 响应结构体指针                                          */
/*           resp_line --- 需要获取数据的行号                                 */
/* 输出参数: 无                                                               */
/* 返 回 值: const char                                                       */
/* 操作流程: != NULL 成功，返回指向响应结构体的指针；  = NULL 失败，行号错误  */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-03-31              1             创建响?    创建函数              */
/*<FUNC->**********************************************************************/
const char *at_resp_get_line(at_response_t resp, rt_size_t resp_line)
{
    char *resp_buf = resp->buf;
    char *resp_line_buf = RT_NULL;
    rt_size_t line_num = 1;

    RT_ASSERT(resp);

    if (resp_line > resp->line_counts || resp_line <= 0)
    {
        LOG_ERR("AT response get line failed! Input response line(%d) error!", resp_line);
        return RT_NULL;
    }
    /* 不是因为第0行是\r\n没必要打印故而从第一行打印，而是因为接收到的响应的行数是从第一行开始计数的 */
    for (line_num = 1; line_num <= resp->line_counts; line_num++)
    {
        if (resp_line == line_num)
        {
            resp_line_buf = resp_buf;

            return resp_line_buf;         /* 遍历到了则返回此行的响应数据，遍历不到则返回空 */
        }

        resp_buf += strlen(resp_buf) + 1; /* 不是预支1B准备存放'\0',而是实际返回的响应数据各行之间是用'\r''\0'隔开的 */
                                          /* 而不是'\r''\n'(0x0D 0x0A), '\r'可由strlen计入长度，但\0却不可，因此要+1 */
    }

    return RT_NULL;  /* 2个return 函数有两个出口 */
}
/*<FUNC+>**********************************************************************/
/* 函数名称: at_resp_get_line_by_kw                                           */
/* 功能描述: 获取关键字信息                                                   */
/* 输入参数: resp --- 响应消息                                                */
/*           keyword --- 关键字                                               */
/* 输出参数: 无                                                               */
/* 返 回 值: != NULL 成功，返回指向响应结构体的指针；= NULL 失败，关键字未找到*/
/* 操作流程:                                                                  */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-01-14              1.            初始化?    创建函数              */
/*<FUNC->**********************************************************************/
const char *at_resp_get_line_by_kw(at_response_t resp, const char *keyword)
{
    char *resp_buf = resp->buf;
    char *resp_line_buf = RT_NULL;
    rt_size_t line_num = 1;

    RT_ASSERT(resp);
    RT_ASSERT(keyword);

    for (line_num = 1; line_num <= resp->line_counts; line_num++)
    {
        if (strstr(resp_buf, keyword))
        {
            resp_line_buf = resp_buf;

            return resp_line_buf;
        }

        resp_buf += strlen(resp_buf) + 1; 
    }

    return RT_NULL;
}
/*<FUNC+>**********************************************************************/
/* 函数名称: at_resp_parse_line_args                                          */
/* 功能描述: 解析指定行号的响应数据                                           */
/* 输入参数: resp --- 响应结构体指针                                          */
/*           resp_line --- 需要解析数据的行号，从1开始                        */
/*           resp_expr --- 自定义的参数解析表达式                             */
/* 输出参数: 无                                                               */
/* 返 回 值: -1 ：输入响应行号错误或获取行缓冲区错误                          */
/*            0 ：解析不匹配                                                  */
/*          > 0 ：成功解析参数的数量                                          */
/* 操作流程:                                                                  */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-03-31              1             创建响?    创建函数              */
/*<FUNC->**********************************************************************/
int at_resp_parse_line_args(at_response_t resp, rt_size_t resp_line, const char *resp_expr, ...)
{
    va_list args;
    int resp_args_num = 0;
    const char *resp_line_buf = RT_NULL;

    RT_ASSERT(resp);
    RT_ASSERT(resp_expr);

    if ((resp_line_buf = at_resp_get_line(resp, resp_line)) == RT_NULL)
    {
        return -1;
    }

    va_start(args, resp_expr);

    resp_args_num = vsscanf(resp_line_buf, resp_expr, args);

    va_end(args);

    return resp_args_num;
}
/*<FUNC+>**********************************************************************/
/* 函数名称: at_resp_parse_line_args_by_kw                                    */
/* 功能描述: 解析指定关键字行的响应数据                                       */
/* 输入参数: resp --- 响应结构体指针                                          */
/*           keyword --- 关键字信息                                           */
/*           resp_expr --- 自定义的参数解析表达式                             */
/*           。。。 解析参数列表，为可变参数                                  */
/* 返 回 值: -1 ：输入参数错误或获取行缓冲区错误                              */
/*            0 ：解析不匹配                                                  */
/*          > 0 ：成功解析参数的数量                                          */
/* 操作流程:                                                                  */
/* 操作流程:                                                                  */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-03-31              1             创建响?    创建函数              */
/*<FUNC->**********************************************************************/
int at_resp_parse_line_args_by_kw(at_response_t resp, const char *keyword, const char *resp_expr, ...)
{
    va_list args;
    int resp_args_num = 0;
    const char *resp_line_buf = RT_NULL;

    RT_ASSERT(resp);
    RT_ASSERT(resp_expr);

    if ((resp_line_buf = at_resp_get_line_by_kw(resp, keyword)) == RT_NULL)
    {
        return -1;
    }

    va_start(args, resp_expr);

    resp_args_num = vsscanf(resp_line_buf, resp_expr, args);

    va_end(args);

    return resp_args_num;
}

/*<FUNC+>**********************************************************************/
/* 函数名称: at_obj_exec_cmd                                                  */
/* 功能描述: 向服务器发送命令并等待响应                                       */
/* 输入参数: client --- 操作句柄                                              */
/*           resp --- 响应内容                                                */
/*           cmd_expr --- 其他参数                                            */
/* 输入参数: 无                                                               */
/* 输出参数: 0：成功，-1：响应错误，-2:等待超时                               */
/* 返 回 值: int                                                              */
/* 操作流程:                                                                  */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-01-14              1.            初始化?    创建函数              */
/*<FUNC->**********************************************************************/
int at_obj_exec_cmd(at_client_t client, at_response_t resp, const char *cmd_expr, ...)
{
    va_list args;
    rt_size_t cmd_size = 0;
    rt_err_t result = RT_EOK;
    const char *cmd = RT_NULL;

    RT_ASSERT(cmd_expr);

    if (client == RT_NULL)
    {
        LOG_ERR("input AT Client object is NULL, please create or get AT Client object!");
        return -RT_ERROR;
    }

    rt_mutex_take(client->lock, RT_WAITING_FOREVER); /* 获取互斥锁 */

    client->resp_status = AT_RESP_OK;
    client->resp = resp;

    va_start(args, cmd_expr);
    at_vprintfln(client->device, cmd_expr, args);   /*  不需要用户发送\r\n  */
    va_end(args);

    if (resp != RT_NULL)
    {
        resp->line_counts = 0;
        if (rt_sem_take(client->resp_notice, resp->timeout) != RT_EOK) /* 在timeout的时间内没收到信号量resp_notice */
        {
            cmd = at_get_last_cmd(&cmd_size); /* 获取上条发送的AT指令及其长度 */
            //LOG_ERR("execute command (%.*s) timeout (%d ticks)!", cmd_size, cmd, resp->timeout);
            client->resp_status = AT_RESP_TIMEOUT;
            result = -RT_ETIMEOUT;
            goto __exit;
        }
        
        /* 疑问：前面并未对client->resp_status进行操作，应该还是AT_RESP_OK       */
        /* 解答: 传递的是指针,在client_parser()中会对client->resp_status进行修改 */
        if (client->resp_status != AT_RESP_OK) 
        {
            cmd = at_get_last_cmd(&cmd_size);
            //LOG_ERR("execute command (%.*s) failed!", cmd_size, cmd);
            result = -RT_ERROR;
            goto __exit;
        }
    }

__exit:
    client->resp = RT_NULL;

    rt_mutex_release(client->lock);  /* 释放互斥锁 */

    return result;
}
/*<FUNC+>**********************************************************************/
/* 函数名称: at_client_obj_wait_connect                                       */
/* 功能描述: 等待模块初始化完成                                               */
/* 输入参数: client --- 客户端操作句柄                                        */
/*           timeout --- 等待超时时间                                         */
/* 输出参数: 无                                                               */
/* 返 回 值:  0 ：成功                                                        */
/*           -2 ：超时                                                        */
/*           -5 ：申请内存识别                                                */
/* 操作流程:                                                                  */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-04-2               1             创建响?    创建函数              */
/*<FUNC->**********************************************************************/
int at_client_obj_wait_connect(at_client_t client, rt_uint32_t timeout)
{
    rt_err_t result = RT_EOK;
    at_response_t resp = RT_NULL;
    rt_tick_t start_time = 0;

    if (client == RT_NULL)
    {
        LOG_ERR("input AT Client object is NULL, please create or get AT Client object!");
        return -RT_ERROR;
    }

    resp = at_create_resp(16, 0, rt_tick_from_millisecond(timeout));
    if (resp == RT_NULL)
    {
        LOG_ERR("No memory for response object!");
        return -RT_ENOMEM;
    }

    rt_mutex_take(client->lock, RT_WAITING_FOREVER);
    client->resp = resp;
    start_time = rt_tick_get();

    while (1)
    {
        /* 检测是否超时 */
        if (rt_tick_get() - start_time > timeout)
        {
            LOG_ERR("wait connect timeout (%d millisecond)!", timeout);
            result = -RT_ETIMEOUT;
            break;
        }

        /* 检测连接是否正常 */
        resp->line_counts = 0;
        rt_device_write(client->device, 0, "AT\r\n", 4);

        if (rt_sem_take(client->resp_notice, resp->timeout) != RT_EOK)
            continue;
        else
            break;
    }

    at_delete_resp(resp);
    client->resp = RT_NULL;
    rt_mutex_release(client->lock);

    return result;
}
/*<FUNC+>**********************************************************************/
/* 函数名称: at_client_obj_send                                               */
/* 功能描述: 发送指定长度数据(最原始的串口发送函数，需发送带\r\n的完整数据)   */
/* 输入参数: client --- 客户端操作句柄                                        */
/*           buf --- 发送数据起始指针                                         */
/*           size --- 发送数据大小                                            */
/* 输出参数: 无                                                               */
/* 返 回 值: >0 :发送数据的大小   =0：发送失                                  */
/* 操作流程:                                                                  */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-04-2               1             创建响?    创建函数              */
/*<FUNC->**********************************************************************/
rt_size_t at_client_obj_send(at_client_t client, const char *buf, rt_size_t size)
{
    RT_ASSERT(buf);

    if (client == RT_NULL)
    {
        LOG_ERR("input AT Client object is NULL, please create or get AT Client object!");
        return 0;
    }

#ifdef AT_PRINT_RAW_CMD
    at_print_raw_cmd("sendline", buf, size);  /* 日志打印 */
#endif

    return rt_device_write(client->device, 0, buf, size); /* 实际串口发送 */
}

/*<FUNC+>**********************************************************************/
/* 函数名称: at_client_getchar                                                */
/* 功能描述: 读取接收到的响应数据                                             */
/* 输入参数: client --- 操作句柄                                              */
/* 输出参数: 无                                                               */
/* 返 回 值: 将接收回调函数指示收到的一帧数据逐个字节的读出到ch里             */
/* 操作流程: 如果此时串口没有数据则读取一个字节放到ch里就会返回0,就会进入while*/
/*           之后就会挂起;当串口收到一帧数据后会释放信号量,此时此函数就会解挂 */
/*           就会开始while循环,由于此时串口中已经收到数了,那么读取一个字节放到*/
/*           ch里就会返回1,那么就不会进入while里被挂起,直到上次串口收到的一帧 */
/*           数据被完全通过ch逐个字节的读完毕后,再多读一个就会返回0,进入while */
/*           里,被挂起                                                        */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-01-14              1.            初始化?    创建函数              */
/*<FUNC->**********************************************************************/
static char at_client_getchar(at_client_t client)
{
    char ch;

    while (rt_device_read(client->device, 0, &ch, 1) == 0)  /* 没读到数据 */
    {
        /* 设置信号量：将接收信号量初值重新设为0 */
        rt_sem_control(client->rx_notice, RT_IPC_CMD_RESET, RT_NULL);
        
        /* 获取信号量,此处会被挂起死等接收回调at_client_rx_ind中释放,而不是一直刷while */
        rt_sem_take(client->rx_notice, RT_WAITING_FOREVER);
    }  

    return ch;
}
/*<FUNC+>**********************************************************************/
/* 函数名称: at_client_obj_recv                                               */
/* 功能描述: 接收指定长度数据                                                 */
/* 输入参数: client --- 客户端操作句柄                                        */
/*           buf --- 接收数据的指针                                           */
/*           size --- 最大支持接收数据的长度                                  */
/* 输出参数: 无                                                               */
/* 返 回 值: >0 成功，返回接收成功的数据长度；  =0：失败                      */
/* 操作流程:                                                                  */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-04-2               1             创建响?    创建函数              */
/*<FUNC->**********************************************************************/
rt_size_t at_client_obj_recv(at_client_t client, char *buf, rt_size_t size)
{
    rt_size_t read_idx = 0;
    char ch;

    RT_ASSERT(buf);

    if (client == RT_NULL)
    {
        LOG_ERR("input AT Client object is NULL, please create or get AT Client object!");
        return 0;
    }

    while (1)
    {
        if (read_idx < size)
        {
            ch = at_client_getchar(client);
            buf[read_idx++] = ch;
        }
        else
        {
            break;
        }
    }

#ifdef AT_PRINT_RAW_CMD
//    at_print_raw_cmd("urc_recv", buf, size);  /* 日志回显 */
#endif

    return read_idx;
}
/*<FUNC+>**********************************************************************/
/* 函数名称: at_obj_set_end_sign                                              */
/* 功能描述: 设置接收数据的行结束符                                           */
/* 输入参数: client --- 当前客户端的操作句柄                                  */
/*           ch --- 行结束符                                                  */
/* 输出参数: 无                                                               */
/* 返 回 值: void                                                             */
/* 操作流程:                                                                  */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-04-2               1             创建响?    创建函数              */
/*<FUNC->**********************************************************************/
void at_obj_set_end_sign(at_client_t client, char ch)
{
    if (client == RT_NULL)
    {
        LOG_ERR("input AT Client object is NULL, please create or get AT Client object!");
        return;
    }

    client->end_sign = ch;
}
/*<FUNC+>**********************************************************************/
/* 函数名称: at_obj_set_urc_table                                             */
/* 功能描述: URC数据列表初始化                                                */
/* 输入参数: client --- 客户端操作句柄                                        */
/*           urc_table --- URC数据结构体数组指针                              */
/*           table_sz --- URC成员个数                                         */
/* 输出参数: 无                                                               */
/* 返 回 值: void                                                             */
/* 操作流程:                                                                  */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-04-2               1             创建响?    创建函数              */
/*<FUNC->**********************************************************************/
void at_obj_set_urc_table(at_client_t client, const struct at_urc *urc_table, rt_size_t table_sz)
{
    rt_size_t idx;

    if (client == RT_NULL)
    {
        LOG_ERR("input AT Client object is NULL, please create or get AT Client object!");
        return;
    }

    for (idx = 0; idx < table_sz; idx++)
    {
        RT_ASSERT(urc_table[idx].cmd_prefix);
        RT_ASSERT(urc_table[idx].cmd_suffix);
    }

    client->urc_table = urc_table;
    client->urc_table_size = table_sz;
}
/*<FUNC+>**********************************************************************/
/* 函数名称: at_client_get                                                    */
/* 功能描述: 通过名字获得客户端结构体                                         */
/* 输入参数: dev_name --- 客户端名字                                          */
/* 输出参数: 无                                                               */
/* 返 回 值: RT_NULL 失败；成功则返回客户端所使用的控制块(串口)               */
/* 操作流程:                                                                  */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-04-2               1             创建响?    创建函数              */
/*<FUNC->**********************************************************************/
at_client_t at_client_get(const char *dev_name)
{
    int idx = 0;

    RT_ASSERT(dev_name);

    for (idx = 0; idx < AT_CLIENT_NUM_MAX; idx++)
    {
        if (rt_strcmp(at_client_table[idx].device->parent.name, dev_name) == 0)
        {
            return &at_client_table[idx];
        }
    }

    return RT_NULL;
}
/*<FUNC+>**********************************************************************/
/* 函数名称: at_client_get_first                                              */
/* 功能描述: 获得当前客户端的响应结构体指针                                   */
/* 输入参数: 无                                                               */
/* 输出参数: 无                                                               */
/* 返 回 值: RT_NULL 列表为空；成功则返回第一个客户端指控制块                 */
/* 操作流程:                                                                  */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-04-2               1             创建响?    创建函数              */
/*<FUNC->**********************************************************************/
at_client_t at_client_get_first(void)
{
    if (at_client_table[0].device == RT_NULL)
    {
        return RT_NULL;
    }

    return &at_client_table[0];
}
/*<FUNC+>**********************************************************************/
/* 函数名称: get_urc_obj                                                      */
/* 功能描述: 解析URC表                                                        */
/* 输入参数: client --- 客户端结构体指针                                      */
/* 输出参数: 无                                                               */
/* 返 回 值: at_urc类型的结构体指针                                           */
/* 操作流程:                                                                  */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-06-18              v1.0          cloud         创建函数           */
/*<FUNC->**********************************************************************/
static const struct at_urc *get_urc_obj(at_client_t client)
{
    rt_size_t i, prefix_len, suffix_len;
    rt_size_t buf_sz;
    char *buffer = RT_NULL;

    if (client->urc_table == RT_NULL)
    {
        return RT_NULL;
    }

    buffer = client->recv_buffer;
    buf_sz = client->cur_recv_len;

    for (i = 0; i < client->urc_table_size; i++)
    {
        prefix_len = strlen(client->urc_table[i].cmd_prefix);
        suffix_len = strlen(client->urc_table[i].cmd_suffix);
        if (buf_sz < prefix_len + suffix_len) /* 接收长度太短，不匹配 */
        {
            continue;
        }
        /**********************************************************************/
        /* 条件表达式:第一个参数值为真则整体表达式取的是第二个参数的计算结果  */
        /* 若第一个参数值为假则整体表达式取的是第三个参数的计算结果。         */
        /* int strncmp(str1, str2, n); str1与str2比较n个字节,如果全等则返回0  */
        /* ：1 是为了当前缀或后缀为空NULL(即不考虑前缀或后缀)时,不至于因为    */
        /* prefix_len或suffix_len==0,而使逻辑与运算的某项为0,即支持判断单个缀 */
        /**********************************************************************/
        if ((prefix_len ? !strncmp(buffer, client->urc_table[i].cmd_prefix, prefix_len) : 1) && \
            (suffix_len ? !strncmp(buffer + buf_sz - suffix_len, client->urc_table[i].cmd_suffix, suffix_len) : 1))
        {           
            return &client->urc_table[i];
        }
    }

    return RT_NULL;
}

/*<FUNC+>**********************************************************************/
/* 函数名称: at_recv_readline                                                 */
/* 功能描述: AT数据接收处理                                                   */
/* 输入参数: client --- 操作句柄                                              */
/* 输出参数: 无                                                               */
/* 返 回 值: 返回读取到的数据长度                                             */
/* 操作流程: 看似每次调用此函数都需开辟recv_buffer空间,其实由于形参client指向 */
/*           了全局变量,recv_buffer空间为全局变量一直存在并占用的固定空间     */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-01-14              1.            初始化?    创建函数              */
/*<FUNC->**********************************************************************/
static int at_recv_readline(at_client_t client)
{
    rt_size_t read_len = 0;
    char ch = 0, last_ch = 0;
    rt_bool_t is_full = RT_FALSE;

    memset(client->recv_buffer, 0x00, client->recv_bufsz); /* 接收buff置空   */
    client->cur_recv_len = 0;                              /* 接收长度清0    */

    while (1)
    {
        ch = at_client_getchar(client);  /* 每次只返回一个字节 */

        if (read_len < client->recv_bufsz)
        {
            /* 从底层串口的环形接收BUF中取出数据转移到AT组件的客户端接收BUF内 */
            client->recv_buffer[read_len++] = ch;  
            client->cur_recv_len = read_len;
        }
        else
        {
            is_full = RT_TRUE;
        }

        /* 判断是新响应行数据还是URC数据，模组响应数据的各行均以\r\n结尾 */
        /* 最后一个判断无意义,因为在client_parser函数中对AT组件接收BUF的所有数据又进行了一次get_urc_obj(client)的判断 */
        if ((ch == '\n' && last_ch == '\r') || (client->end_sign != 0 && ch == client->end_sign) || get_urc_obj(client))
        {
            if (is_full)
            {
                LOG_ERR("read line failed. The line data length is out of buffer size(%d)!", client->recv_bufsz);
                memset(client->recv_buffer, 0x00, client->recv_bufsz);
                client->cur_recv_len = 0;
                return -RT_EFULL;
            }
            break;
        }
        last_ch = ch;
    }

#ifdef AT_PRINT_RAW_CMD
    if(!HTTP_FLAG)
    {
        at_print_raw_cmd("recvline", client->recv_buffer, read_len); /* 日志回显 */
    }
#endif

    return read_len;
}

/*<FUNC+>**********************************************************************/
/* 函数名称: client_parser  (客户端语法分析器) (AT线程执行函数)               */
/* 功能描述: AT线程回调函数   客户端数据解析  用来解析各响应行数据            */
/* 输入参数: client --- 操作句柄                                              */
/* 输出参数: 无                                                               */
/* 返 回 值: static void                                                      */
/* 操作流程: 将底层串口接收的数据先转移到client->recv_buffer(每次只存放一行   */
/*           响应)中，再从client->recv_buffer转移到client->resp->buf中        */
/* 其它说明: 以响应中的\r\n界定行,同时为了方便对各响应行的操作,将\r\n变为\r\0 */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-01-12              1.            初始化?    创建函数              */
/*<FUNC->**********************************************************************/
static void client_parser(at_client_t client)
{
    int resp_buf_len = 0;
    const struct at_urc *urc;
    rt_size_t line_counts = 0;

    while(1)
    {
        /* 将底层串口的数据转移到client->recv_buffer中,数据长度client->cur_recv_len */
        if (at_recv_readline(client) > 0)  
        {
            if ((urc = get_urc_obj(client)) != RT_NULL)/* 返回URC结构指针 */
            {
                /* 当前内容存在回调函数，则执行回调函数 */
                if (urc->func != RT_NULL)
                {
                    urc->func(client->recv_buffer, client->cur_recv_len);
                }
            }
            /* 非URC数据时对原始响应数据做了调整由\r\n变为\r\0，而之上的URC原始数据不需要调整 */
            else if (client->resp != RT_NULL)
            {
                if(!HTTP_FLAG) /* HTTP模式下禁止转换 */
                {
                    /* 将模组返回的响应数据的行结束符由\r\n变为\r\0,用于处理各行响应时有终止符 */
                    client->recv_buffer[client->cur_recv_len - 1] = '\0';  /* \n --> \0 */
                }
                
                /* 累计各响应行数据总长须小于响应结构体中开的收数buf，且client->recv_buffer每次只收取一行响应 */
                if (resp_buf_len + client->cur_recv_len < client->resp->buf_size)
                {
                    /* 复制响应行，由'\0'分割;将各响应行按序存放至响应结构体中开的收数buf中,实际非\0数据长度小于待拷贝长度时多余的补\0 */
                    memcpy(client->resp->buf + resp_buf_len, client->recv_buffer, client->cur_recv_len);
                    /* 将client->recv_buffer中的各响应行转移到响应结构体中开的收数buf中 */
                    resp_buf_len += client->cur_recv_len; 
                    /* 行号从1开始计数，即使只有一行，其行号为1，而非0 */
                    line_counts++;  
                }
                else /* 累计的各响应行数据总长超client->resp->buf_size */
                {
                    client->resp_status = AT_RESP_BUFF_FULL;
                    LOG_ERR("Read response buffer failed. The Response buffer size is out of buffer size(%d)!", client->resp->buf_size);
                }
                
                /* 检查各响应行,如果某行响应是以字符OK开始的，且响应结构体中的本次响应数据需要接收的行数为0时,if成立 */
                if (memcmp(client->recv_buffer, AT_RESP_END_OK, strlen(AT_RESP_END_OK)) == 0 && client->resp->line_num == 0)
                {
                    client->resp_status = AT_RESP_OK; /* 根据响应数据返回响应结果状态 */
                    if(HTTP_FLAG) HTTP_FLAG = 0;      /* 还原HTTP标志位 */
                }
                /* 检查各响应行,如果某行响应中含有字符ERROR，或以字符FAIL开始的,则判断成立 */
                else if (strstr(client->recv_buffer, AT_RESP_END_ERROR) || (memcmp(client->recv_buffer, AT_RESP_END_FAIL, strlen(AT_RESP_END_FAIL)) == 0))
                {
                    client->resp_status = AT_RESP_ERROR;
                }
                /* 如果某行的行号等于响应结构体中设置的期望接收行数时，判断成立 */
                else if (line_counts == client->resp->line_num && client->resp->line_num)
                {
                    client->resp_status = AT_RESP_OK;
                }
                /* 此前接收的所有响应行没有以OK或FAIL开头,也不含ERROR,行数也不够;即前面还未返回响应关键字,继续接收响应行 */
                else
                {
                    continue; /* 直接跳至 if(at_recv_readline(client)>0),而不执行此行下的语句 */
                }
                /* 当接收到响应关键字或行数够时,某条AT指令的响应接收完毕 */
                client->resp->line_counts = line_counts;  /* 某条AT指令响应接收完成时共接收的响应总行数 */
                client->resp = RT_NULL;               /* 上条AT指令响应接收完成后指空,准备指向下条AT指令对应的响应结构体指针 */
                rt_sem_release(client->resp_notice);  /* 某条AT指令响应接收完成时释放一个响应信号量 */
                resp_buf_len = 0, line_counts = 0;    /* 各响应行在某条AT指令总响应中的偏移和响应行计数清零,为下条AT指令做准备 */
            }
            else /* client->resp == RT_NULL，未经请求下发的响应数据(URC) */
            {
                //LOG_LOG("unrecognized line: %.*s", client->cur_recv_len, client->recv_buffer);
            }
        }
    }
}

/*<FUNC+>**********************************************************************/
/* 函数名称: at_client_rx_ind                                                 */
/* 功能描述: 接收回调函数                                                     */
/* 输入参数: dev --- 句柄                                                     */
/*           size --- 大小                                                    */
/* 输出参数: 无                                                               */
/* 返 回 值: static rt_err_t                                                  */
/* 操作流程:                                                                  */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-01-12              1.            初始化?    创建函数              */
/*<FUNC->**********************************************************************/
static rt_err_t at_client_rx_ind(rt_device_t dev, rt_size_t size)
{
    int idx = 0;
    /* 确定数据触发的设备，并释放对应的信号量 */
    for (idx = 0; idx < AT_CLIENT_NUM_MAX; idx++)
    {
        if (at_client_table[idx].device == dev && size > 0)
        {
            rt_sem_release(at_client_table[idx].rx_notice); /* 释放接收信号量 */
        }
    }

    return RT_EOK;
}
/*<FUNC+>**********************************************************************/
/* 函数名称: at_client_para_init                                              */
/* 功能描述: 初始化客户端对象参数                                             */
/* 输入参数: client --- 客户端句柄                                            */
/* 输出参数: 无                                                               */
/* 返 回 值: static int                                                       */
/* 操作流程:                                                                  */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-01-12              1.            初始化?    创建函数              */
/*<FUNC->**********************************************************************/
static int at_client_para_init(at_client_t client)
{
    #define  AT_CLIENT_LOCK_NAME       "at_c"            /* 互斥量名字 */
    #define  AT_CLIENT_SEM_NAME        "at_cs"           /* 信号量名字 */
    #define  AT_CLIENT_RESP_NAME       "at_cr"           /* 响应名字   */
    #define  AT_CLIENT_THREAD_NAME     "at_clnt"         /* 线程名字   */

    int result = RT_EOK;
    static int at_client_num = 0;
    char name[RT_NAME_MAX];
    
    client->status = AT_STATUS_UNINITIALIZED;  /* 状态赋值为未初始化状态 */
    client->cur_recv_len = 0;                  /* 初始接收长度为0        */
    
    /* AT组件接收数据缓存空间内存申请 */
    client->recv_buffer = (char *) rt_calloc(1, client->recv_bufsz);
    if (client->recv_buffer == RT_NULL)
    {
        LOG_ERR("AT client initialize failed! No memory for receive buffer.")
        result = -RT_ENOMEM;
        goto __exit;
    }
    /* 字符串格式化，互斥量注册, name = "at_c0" */
    rt_snprintf(name, RT_NAME_MAX, "%s%d", AT_CLIENT_LOCK_NAME, at_client_num);
    client->lock = rt_mutex_create(name, RT_IPC_FLAG_FIFO);
    if (client->lock == RT_NULL)
    {
        LOG_ERR("AT client initialize failed! at_client_recv_lock create failed!");
        result = -RT_ENOMEM;
        goto __exit;
    }
    /* 接收信号量初始化, name = "at_cs0" */
    rt_snprintf(name, RT_NAME_MAX, "%s%d", AT_CLIENT_SEM_NAME, at_client_num);
    client->rx_notice = rt_sem_create(name, 0, RT_IPC_FLAG_FIFO);  /* 初始只有0个信号量(没有可用的) */
    if (client->rx_notice == RT_NULL)
    {
        LOG_ERR("AT client initialize failed! at_client_notice semaphore create failed!");
        result = -RT_ENOMEM;
        goto __exit;
    }
    /* 响应信号量初始化, name = "at_cr0" */
    rt_snprintf(name, RT_NAME_MAX, "%s%d", AT_CLIENT_RESP_NAME, at_client_num);
    client->resp_notice = rt_sem_create(name, 0, RT_IPC_FLAG_FIFO);
    if (client->resp_notice == RT_NULL)
    {
        LOG_ERR("AT client initialize failed! at_client_resp semaphore create failed!");
        result = -RT_ENOMEM;
        goto __exit;
    }

    client->urc_table = RT_NULL;
    client->urc_table_size = 0;
    /* 线程创建  AT优先级=2, name = "at_clnt0" */
    rt_snprintf(name, RT_NAME_MAX, "%s%d", AT_CLIENT_THREAD_NAME, at_client_num);
    client->parser = rt_thread_create(name, (void (*)(void *parameter))client_parser,
                                      client, 1024 + 1024, 2, 5);  /* 因为会用到一个1024B的全局数组send_buf */
    if (client->parser == RT_NULL)
    {
        result = -RT_ENOMEM;
        goto __exit;
    }

__exit:
    /* 如果创建线程失败，把之前初始化的信号量 互斥量删除 申请内存释放 */
    if (result != RT_EOK)
    {
        if (client->lock)
        {
            rt_mutex_delete(client->lock);
        }

        if (client->rx_notice)
        {
            rt_sem_delete(client->rx_notice);
        }

        if (client->resp_notice)
        {
            rt_sem_delete(client->resp_notice);
        }

        if (client->device)
        {
            rt_device_close(client->device);
        }

        if (client->recv_buffer)
        {
            rt_free(client->recv_buffer);
        }

        rt_memset(client, 0x00, sizeof(struct at_client));
    }
    else
    {
        at_client_num++;
    }

    return result;
}
/*<FUNC+>**********************************************************************/
/* 函数名称: at_client_init                                                   */
/* 功能描述: AT 客户端初始化                                                  */
/* 输入参数: dev_name --- 设备名称                                            */
/*           recv_bufsz --- 接收数据的最大长度                                */
/* 输出参数: 无                                                               */
/* 返 回 值: 0 成功 -1 失败  -5 没有空间                                      */
/* 操作流程:                                                                  */
/* 其它说明: 无                                                               */
/* 修改记录:                                                                  */
/* -------------------------------------------------------------------------- */
/*     2019-01-12              1             GYY           创建函数           */
/*<FUNC->**********************************************************************/
int at_client_init(const char *dev_name,  rt_size_t recv_bufsz)
{
    int idx = 0;
    int result = RT_EOK;
    rt_err_t open_result = RT_EOK;
    at_client_t client = RT_NULL;

    RT_ASSERT(dev_name);
    RT_ASSERT(recv_bufsz);

    /* 计算设备数量,初始时at_client_table[idx].device == NULL,所以idx==0 */
    for (idx = 0; idx < AT_CLIENT_NUM_MAX && at_client_table[idx].device; idx++);

    if (idx >= AT_CLIENT_NUM_MAX)
    {
        LOG_ERR("AT client initialize filed! Check the maximum number(%d) of AT client.", AT_CLIENT_NUM_MAX);
        result = -RT_EFULL;
        goto __exit;
    }
    
    client = &at_client_table[idx];
    client->recv_bufsz = recv_bufsz;

    client->device = rt_device_find(dev_name);  /* 查找设备并进行打开操作 */
    if (client->device)
    {
        /* 检查设备类型是否合法  是否为字符型设备 */
        RT_ASSERT(client->device->type == RT_Device_Class_Char);

        /* 驱动打开 */
        open_result = rt_device_open(client->device, RT_DEVICE_OFLAG_RDWR | RT_DEVICE_FLAG_DMA_RX);
        if (open_result == -RT_EIO)
        {   /* 不支持DMA模式时使用中断模式 */
            open_result = rt_device_open(client->device, RT_DEVICE_OFLAG_RDWR | RT_DEVICE_FLAG_INT_RX);
        }
        RT_ASSERT(open_result == RT_EOK);
        
        rt_device_set_rx_indicate(client->device, at_client_rx_ind); /* 设置驱动回调函数 */
    }
    else
    {
        LOG_ERR("AT client initialize failed! Not find the device(%s).", dev_name);
        result = -RT_ERROR;
        goto __exit;
    }

    /* 虽然client是局部变量,但at_client_table[]是全局的,通过指针在修改局量的时候就 */
    /* 修改了全局的数据. 因此client->recv_buffer所指向的已开辟的内存空间将一直存在 */
    result = at_client_para_init(client);  /* AT设备初始化 */
    if (result != RT_EOK)
    {
        goto __exit;
    }

__exit:
    /* 如果初始化成功，则状态切换为初始化状态并启动线程 */
    if (result == RT_EOK)
    {
        client->status = AT_STATUS_INITIALIZED;
        rt_thread_startup(client->parser);
        LOG_INFO("AT client(V%s) on device %s initialize success.", AT_SW_VERSION, dev_name);
    }
    else
    {
        LOG_ERR("AT client(V%s) on device %s initialize failed(%d).", AT_SW_VERSION, dev_name, result);
    }

    return result;
}


